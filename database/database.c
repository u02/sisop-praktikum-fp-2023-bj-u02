#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <stdbool.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <time.h>

#define PORT 8888
#define BUFFER_SIZE 1024

#define MAX_INPUT_LENGTH 100
#define MAX_WORD_LENGTH 20
#define CMD_LENGTH 1000
const char* databasesFolder = "databases";
const char* adminDB = "admin";
const char* adminDB_users = "users";
const char* adminDB_accesses = "accesses";

char current_db[100] = "";
char current_user[100] = "";
char response_msg[100] = "";
int is_super_user = 1;

///////////////////////////////////////////// LINKED LIST PART /////////////////////////////////////////////////////
// Modified from: https://github.com/AlproITS/StrukturData/blob/master/For%20C/D.Array%2C%20Stack%2C%20Queue%2C%20Deque%2C%20Pr.Queue/list_singly.c

            /* Struktur Node */

            typedef struct snode_t {
                char *data;
                char *type;
                struct snode_t *next;
            } SListNode;

            /* Struktur ADT SinglyList */

            typedef struct slist_t {
                unsigned _size;
                SListNode *_head;
            } SinglyList;

            /* DAFTAR FUNGSI YANG TERSEDIA */

            void slist_init(SinglyList *list);
            bool slist_isEmpty(SinglyList *list);
            void slist_pushFront(SinglyList *list, char* value, char* type);
            void slist_popFront(SinglyList *list);
            void slist_pushBack(SinglyList *list, char* value, char* type);
            void slist_popBack(SinglyList *list);
            void slist_insertAt(SinglyList *list, int index, char* value);
            void slist_remove_column(SinglyList *list, char* value);
            void slist_remove(SinglyList *list, char* value);
            int  slist_front(SinglyList *list);
            int  slist_back(SinglyList *list);
            int  slist_getAt(SinglyList *list, int index);

            /* Function definition below */

            void slist_init(SinglyList *list) 
            {
                list->_head = NULL;
                list->_size = 0;
            }

            bool slist_isEmpty(SinglyList *list) {
                return (list->_head == NULL);
            }

            void slist_pushFront(SinglyList *list, char *value, char* type) 
            {
                SListNode *newNode = (SListNode*) malloc(sizeof(SListNode));
                if (newNode) {
                    list->_size++;
                    newNode->data = value;
                    newNode->type = type;

                    if (slist_isEmpty(list)) newNode->next = NULL;
                    else newNode->next = list->_head;
                    list->_head = newNode;
                }
            }

            void slist_popFront(SinglyList *list)
            {
                if (!slist_isEmpty(list)) {
                    SListNode *temp = list->_head;
                    list->_head = list->_head->next;
                    list->_size--;
                }
            }

            void slist_pushBack(SinglyList *list, char *value, char* type)
            {
                SListNode *newNode = (SListNode*) malloc(sizeof(SListNode));
                if (newNode) {
                    list->_size++;
                    newNode->data = value;
                    newNode->type = type;
                    newNode->next = NULL;
                    
                    if (slist_isEmpty(list)) 
                        list->_head = newNode;
                    else {
                        SListNode *temp = list->_head;
                        while (temp->next != NULL) 
                            temp = temp->next;
                        temp->next = newNode;
                    }
                }
            }

            void slist_popBack(SinglyList *list)
            {
                if (!slist_isEmpty(list)) {
                    SListNode *nextNode = list->_head->next;
                    SListNode *currNode = list->_head;

                    if (currNode->next == NULL) {
                        list->_head = NULL;
                        return;
                    }

                    while (nextNode->next != NULL) {
                        currNode = nextNode;
                        nextNode = nextNode->next;
                    }
                    currNode->next = NULL;
                    list->_size--;
                }
            }

            // TODO
            void slist_remove_column(SinglyList *list, char* value)
            {
                if (!slist_isEmpty(list)) {
                    SListNode *temp = list->_head;
                    int _i = 0;
                    while (temp->next != NULL) {

                        temp = temp->next;
                        _i++;
                    }
                    SListNode *nextTo = temp->next->next;
                    temp->next = nextTo;
                    list->_size--;
                }
            }

            void slist_remove(SinglyList *list, char* value)
            {
                if (!slist_isEmpty(list)) {
                    SListNode *temp, *prev;
                    temp = list->_head;

                    if (temp->data == value) {
                        slist_popFront(list);
                        return;
                    }
                    while (temp != NULL && temp->data != value) {
                        prev = temp;
                        temp = temp->next;
                    }

                    if (temp == NULL) return;
                    prev->next = temp->next;
                    list->_size--;
                }
            }

///////////////////////////////////////////// LINKED LIST PART /////////////////////////////////////////////////////

void sanitizeString(char* str) {
    int length = strlen(str);
    int j = 0;

    for (int i = 0; i < length; i++) {
        if (isprint(str[i])) {
            str[j] = str[i];
            j++;
        }
    }

    str[j] = '\0';
}

int has_access(char* database) {
    FILE* file = fopen("databases/admin/accesses", "r");
    if (file == NULL) {
        return 0;
    }

    char line[100];
    int accessGranted = 0;

    while (fgets(line, sizeof(line), file) != NULL) {
        char* username = strtok(line, ":");
        char* db = strtok(NULL, "\n");

        if (username != NULL && db != NULL && strcmp(username, current_user) == 0 && strcmp(db, database) == 0) {
            accessGranted = 1;
            break;
        }
    }

    fclose(file);

    return accessGranted;

}

int login(char* username, char* password) {
    FILE* file = fopen("databases/admin/users", "r");
    if (file == NULL) {
        return 0;
    }

    char line[100];
    int loginSuccessful = 0;

    while (fgets(line, sizeof(line), file) != NULL) {
        char* storedUsername = strtok(line, ":");
        char* storedPassword = strtok(NULL, "\n");

        if (storedUsername != NULL && storedPassword != NULL && strcmp(storedUsername, username) == 0 &&
            strcmp(storedPassword, password) == 0) {
            loginSuccessful = 1;
            break;
        }
    }

    fclose(file);

    return loginSuccessful;
}

void get_column_name_n_type(char* unparsed, char* name, char* type){
    const char* delimiter = "=";

    const char* delimiterPos = strstr(unparsed, delimiter);
    if (delimiterPos == NULL) {
        return 0;
    }

    // Calculate the lengths of the two substrings
    size_t delimiterIndex = delimiterPos - unparsed;
    size_t firstStringLength = delimiterIndex;
    size_t secondStringLength = strlen(unparsed) - delimiterIndex - strlen(delimiter);

    // Allocate memory for the two substrings
    char* firstString = (char*)malloc((firstStringLength + 1) * sizeof(char));
    char* secondString = (char*)malloc((secondStringLength + 1) * sizeof(char));

    // Copy the first substring
    memcpy(firstString, unparsed, firstStringLength);
    firstString[firstStringLength] = '\0';

    // Copy the second substring
    memcpy(secondString, delimiterPos + strlen(delimiter), secondStringLength);
    secondString[secondStringLength] = '\0';

    strcpy(name, firstString);
    strcpy(type, secondString);
}

int get_table_structure(SinglyList* list, char* table_name) {
    char filePath[100];
    snprintf(filePath, 100,"%s/%s/%s", databasesFolder, current_db, table_name);

    FILE* file = fopen(filePath, "r");

    if (file == NULL) {
        return -1;
    }

    char line[500];
    if (fgets(line, 500, file) != NULL) {
        char* token = strtok(line, ":");
        int i=0;
        while (token != NULL) {

            i++;
            char c_name[100], c_type[100];
            get_column_name_n_type(token, c_name, c_type);

            slist_pushBack(list, c_name, c_type);
            
            token = strtok(NULL, ":");
        }
    } else {
        return -1;
    }
}

int is_string(char* str){
    size_t length = strlen(str);

    if (length < 2) {
        return 0; // String is too short to have quotes at both ends
    }

    return (str[0] == '\'') && (str[length - 1] == '\'');
}

int create_user(char* token) {
    char username[100], password[100];

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(username, token);

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    if(strcmp(token, "IDENTIFIED")!=0) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    if(strcmp(token, "BY")!=0) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    token = strtok(NULL, " ");
    if(token==NULL){
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(password, token);

    // Removing the Semicolon
    int len = strlen(password);
    if (len > 0 && password[len - 1] == ';') {
        password[len - 1] = '\0';
    }

    if (!is_super_user){
        strcpy(response_msg, "Only Super User can create account!");
        return -1;
    }

    char file_users[100];
    sprintf(file_users, "%s/%s/%s", databasesFolder, adminDB, adminDB_users);

    FILE* file = fopen(file_users, "a");
    if (file == NULL) {
        strcpy(response_msg, "Error, Please try again later!");
        return -1;
    }

    char user_new[100];
    sprintf(user_new, "\n%s:%s", username, password);
    fprintf(file, "%s\n", user_new);
    fclose(file);

    strcpy(response_msg, "User Created!");
    return 0;
}

int create_db(char* token) {
    char db_name[100];

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    strcpy(db_name, token);

    // Removing the Semicolon
    int len = strlen(db_name);
    if (len > 0 && db_name[len - 1] == ';') {
        db_name[len - 1] = '\0';
    }

    // Open the 'databases' folder
    DIR* dir = opendir(databasesFolder);
    if (dir) {
        // Check if the db folder exists
        DIR* new_db_dir = opendir(db_name);
        if (new_db_dir) {
            closedir(new_db_dir);
            strcpy(response_msg, "The Database already exists!");
            return -1;
        } else {
            char newDBpath[100];
            snprintf(newDBpath, sizeof(newDBpath), "%s/%s", databasesFolder, db_name);

            if (mkdir(newDBpath, 0777) == 0) {
                strcpy(response_msg, "The database created successfully!");
                return 0;
            } else {
                strcpy(response_msg, "Failed to create database, try again later!");
                return -1;
            }
        }

        closedir(dir);
    } else {
        strcpy(response_msg, "Failed to create database, try again later!");
        return -1;    
    }

}

int create_table(char* token) {
    char table_name[100], table_base[100] = "";

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(table_name, token);

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    SinglyList table_structure;
    slist_init(&table_structure);

    if(token[0]=='('){
        int i, j;
        for (i = 0, j = 0; token[i] != '\0'; i++) {
            if (token[i] != '(') {
                token[j] = token[i];
                j++;
            }
        }
        token[j] = '\0';

        strcat(table_base, token);

        token = strtok(NULL, " ");
        if(token==NULL) {
            strcpy(response_msg, "Unknown Command!");
            return -1;
        }

        while(token[strlen(token) - 1] != ';') {
            int len = strlen(token);
            if (len > 0 && token[len - 1] == ',') {
                token[len - 1] = '\0';
            }
            strcat(table_base, "=");
            strcat(table_base, token);
            strcat(table_base, ":");

            token = strtok(NULL, " ");
            if(token==NULL) {
                strcpy(response_msg, "Unknown Command!");
                return -1;
            }
            strcat(table_base, token);

            token = strtok(NULL, " ");
            if(token==NULL) {
                strcpy(response_msg, "Unknown Command!");
                return -1;
            }
        }

        int len = strlen(token);
        if (len > 0 && token[len - 1] == ';') {
            token[len - 1] = '\0';
        }

        len = strlen(token);
        if (len > 0 && token[len - 1] == ')') {
            token[len - 1] = '\0';
        }

        strcat(table_base, "=");
        strcat(table_base, token);

        char tableFilePath[100];
        snprintf(tableFilePath, sizeof(tableFilePath), "%s/%s/%s", databasesFolder, current_db, table_name);

        FILE* tableFilePtr = fopen(tableFilePath, "w");
        if (tableFilePtr) {
            fclose(tableFilePtr);
            FILE* file = fopen(tableFilePath, "a");
            if (file == NULL) {
                strcpy(response_msg, "Unknown Command!");
                return -1;
            }

            fprintf(file, "%s\n", table_base);
            fclose(file);
        }
        
        strcpy(response_msg, "Table created successfully!");
        return 0;
    } else {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    return 0;
}

int use_db(char* token) {
    char db_name[100];

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(db_name, token);

    // Removing the Semicolon
    int len = strlen(db_name);
    if (len > 0 && db_name[len - 1] == ';') {
        db_name[len - 1] = '\0';
    }

    if (!has_access(db_name)){
        if(!is_super_user) {
            strcpy(response_msg, "Only Super User or Granted User can access!");
            return -1;
        }
    }

    // Check if the db folder exists
    char path[500];
    snprintf(path, 500,"%s/%s", databasesFolder, db_name);
    printf("%s %s\n", path, db_name);

    DIR* new_db_dir = opendir(path);
    if (new_db_dir) {
        closedir(new_db_dir);
        strcpy(response_msg, "Using the database!");
        strcpy(current_db, db_name);
        return 0;
    } else {
        strcpy(response_msg, "The database doesn't exist!");
        return -1;
    }
}

int grant_permission(char* token){
    char db_name[100], username[100];

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    if(strcmp(token, "PERMISSION") != 0) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(db_name, token);

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    if(strcmp(token, "INTO") != 0) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(username, token);

    // Removing the Semicolon
    int len = strlen(username);
    if (len > 0 && username[len - 1] == ';') {
        username[len - 1] = '\0';
    }

    if (!is_super_user){
        strcpy(response_msg, "Only Super User can grant access!");
        return -1;
    }

    char file_accesses[100];
    sprintf(file_accesses, "%s/%s/%s", databasesFolder, adminDB, adminDB_accesses);

    FILE* file = fopen(file_accesses, "a");
    if (file == NULL) {
        strcpy(response_msg, "Error, please try again later!");
        return -1;
    }

    char access_new[100];
    sprintf(access_new, "%s:%s", username, db_name);
    fprintf(file, "%s\n", access_new);
    fclose(file);

    strcpy(response_msg, "Permission has been granted!");
    return 0;
}

void drop_db(char* token) {
    char db_name[100];

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(db_name, token);

    // Removing the Semicolon
    int len = strlen(db_name);
    if (len > 0 && db_name[len - 1] == ';') {
        db_name[len - 1] = '\0';
    }

    // Create the command to remove the directory
    char command[100];
    snprintf(command, sizeof(command), "rm -r %s/%s", databasesFolder, db_name);

    // Execute the command
    int result = system(command);
    if (result == -1) {
        return 1;
    }

    strcpy(response_msg, "Database has been dropped!");
    return 0;
}

void drop_table(char* token) {
    char table_name[100];

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(table_name, token);

    // Removing the Semicolon
    int len = strlen(table_name);
    if (len > 0 && table_name[len - 1] == ';') {
        table_name[len - 1] = '\0';
    }

    char filePath[100];
    snprintf(filePath, sizeof(filePath), "%s/%s/%s", databasesFolder, current_db, table_name);

    int result = remove(filePath);
    if (result != 0) {
        strcpy(response_msg, "Table Doesnt exist!");
        return -1;
    }
    return 0;
}

void drop_column(char* token) {
    char table_name[100], column_name[100];

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(column_name, token);

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    if(strcmp(token, "FROM") != 0) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(table_name, token);

    // Removing the Semicolon
    int len = strlen(table_name);
    if (len > 0 && table_name[len - 1] == ';') {
        table_name[len - 1] = '\0';
    }

    char filePath[100];
    snprintf(filePath, 100,"%s/%s/%s", databasesFolder, current_db, table_name);

    FILE* file = fopen(filePath, "r");

    if (file == NULL) {
        strcpy(response_msg, "Failed to open the table, please try again later!.");
        return -1;
    }

    char line[500];
    if (fgets(line, 500, file) != NULL) {
        char* token = strtok(line, ":");
        int i=0;
        while (token != NULL) {

            i++;
            char firstWord[100];
            int j;

            for (j = 0; token[j] != '=' && token[j] != '\0'; j++) {
                firstWord[j] = token[j];
            }

            firstWord[j] = '\0';

            if(!strcmp(firstWord, column_name)) {
                char cmd[100];
                snprintf(cmd, 100,"awk -F: '{$%d=\"\"; sub(/::/,\":\"); sub(/  /,\" \")}1' %s/%s/%s > temp.odb", i, databasesFolder, current_db, table_name);

                system(cmd);

                snprintf(cmd, 100,"sed 's/ /:/g' temp.odb > temp2.odb", databasesFolder, current_db, table_name);
                system(cmd);

                snprintf(cmd, 100, "awk '{gsub(/(^:|:$)/, \"\")}1' temp2.odb > %s/%s/%s", databasesFolder, current_db, table_name);
                system(cmd);
                remove("temp.odb");
                remove("temp2.odb");

                break;
            }
            
            token = strtok(NULL, ":");
        }
    } else {
        strcpy(response_msg, "Error, please make sure the table or the column exists!");
        return -1;
    }

    fclose(file);
    strcpy(response_msg, "Successfully drop the column!");
    return 0;
}

void insert(char* token) {
    char table_name[100], row[100] = "\n";

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    if(strcmp(token, "INTO") != 0) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(table_name, token);

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    SinglyList table_structure, types;
    slist_init(&table_structure);
    slist_init(&types);

    if(token[0]=='('){
        int i, j;                

        for (i = 0, j = 0; token[i] != '\0'; i++) {
            if (token[i] != '(') {
                token[j] = token[i];
                j++;
            }
        }
        token[j] = '\0';
        int len = strlen(token);

        if (len > 0 && token[len - 1] == ',') {
            token[len-1] = '\0';
        }

        if(is_string(token)) {
            slist_pushBack(&types, token, "string");
        } else {
            slist_pushBack(&types, token, "int");
        }

        strcat(row, token);
        strcat(row, ":");

        token = strtok(NULL, " ");
        if(token==NULL) {
            strcpy(response_msg, "Unknown Command!");
            return -1;
        }

        while(token[strlen(token) - 1] != ';') {

            if(is_string(token)) {
                slist_pushBack(&types, token, "string");
            } else {
                slist_pushBack(&types, token, "int");
            }

            len = strlen(token);
            if (len > 0 && token[len - 1] == ',') {
                token[len - 1] = '\0';
            }
           
            strcat(row, token);
            strcat(row, ":");

            token = strtok(NULL, " ");
            if(token==NULL) {
                strcpy(response_msg, "Unknown Command!");
                return -1;
            }
        }

        len = strlen(token);
        if (len > 0 && token[len - 1] == ';') {
            token[len - 1] = '\0';
        }

        len = strlen(token);
        if (len > 0 && token[len - 1] == ')') {
            token[len - 1] = '\0';
        }

        if(is_string(token)) {
            slist_pushBack(&types, token, "string");
        } else {
            slist_pushBack(&types, token, "int");
        }
        
        strcat(row, token);

        char tableFilePath[100];
        snprintf(tableFilePath, sizeof(tableFilePath), "%s/%s/%s", databasesFolder, current_db, table_name);
        FILE* tableFilePtr = fopen(tableFilePath, "a");
        if (tableFilePtr) {
            fclose(tableFilePtr);
            FILE* file = fopen(tableFilePath, "a+");
            if (file == NULL) {
                strcpy(response_msg, "Error, please try again later!");
                return -1;
            }

            fprintf(file, "%s\n", row);
            fclose(file);
        }
        
        strcpy(response_msg, "Data inserted!");
        return 0;
    } else {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }    
}

void update(char* token) {
    char table_name[100], column_name[100], column_identifier[100], column_value[100], identifier_value[100];

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(table_name, token);

    token = strtok(NULL, " ");
    if(token==NULL) {
        return -1;
    }
    if(strcmp(token, "SET") != 0) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    int position = -1;
    for (int i = 0; i < strlen(token); i++) {
        if (token[i] == '=') {
            position = i;
            break;
        }
    }
    if (position != -1) {
        // Extract the key and value parts
        strncpy(column_name, token, position);
        column_name[position] = '\0';

        strncpy(column_value, token + position + 1, sizeof(column_value));
        column_value[sizeof(column_value) - 1] = '\0';

    } else {
        strcpy(response_msg, "Error, please try again later!");
        return -1;
    }

    token = strtok(NULL, " ");
    if(token==NULL) {
        // Removing the Semicolon
        int len = strlen(column_value);
        if (len > 0 && column_value[len - 1] == ';') {
            column_value[len - 1] = '\0';
        }

        char fpath[100], fpathnew[100];
        snprintf(fpath, 100, "%s/%s/%s", databasesFolder, current_db, table_name);
        snprintf(fpathnew, 100, "%s/%s/%snew", databasesFolder, current_db, table_name);
        
        FILE* file = fopen(fpath, "r"); // Open the file for reading
        FILE* tempFile = fopen(fpathnew, "w"); // Open a temporary file for writing

        if (file == NULL || tempFile == NULL) {
            strcpy(response_msg, "Error, make sure you are using the correct database!");
            remove(fpathnew); // Remove the original file
            return 1;
        }

        char line[500];
        fgets(line, sizeof(line), file);
        fputs(line, tempFile); // Write the line to the temporary file

        token = strtok(strdup(line), ":"); // Split the columns by ':'
        int columnNumberTarget = 1;
        while (token != NULL) {
            char* equalsSign = strstr(token, "="); // Find the position of '=' in the column

            if (equalsSign != NULL) {
                *equalsSign = '\0'; // Replace '=' with '\0' to separate the column name

                if (strcmp(token, column_name) == 0) {
                    break;
                }
            }

            token = strtok(NULL, ":");
            columnNumberTarget++;
        }
        
        while (fgets(line, sizeof(line), file)) {
            char* token = strtok(line, ":"); // Split the line by ':'
            int columnNumber = 1;
            char modifiedLine[500] = "";

                // Check if the line is empty
            if (strlen(line) == 0 || line[0] == '\n') continue;

            while (token != NULL) {
                if (columnNumber == columnNumberTarget) {
                    strcat(modifiedLine, column_value); // Update the second column value to 99
                } else {
                    strcat(modifiedLine, token); // Keep the original column value
                }


                token = strtok(NULL, ":");
                if(token!=NULL)strcat(modifiedLine, ":"); // Add ':' delimiter
                columnNumber++;
            }

            strcat(modifiedLine, "\n");
            fputs(modifiedLine, tempFile);
        }

        fclose(file);
        fclose(tempFile);

        remove(fpath); // Remove the original file
        rename(fpathnew, fpath); // Rename the temporary file to the original filename
        strcpy(response_msg, "Data Updated!");
        return 0;
    } else if(!strcmp(token, "WHERE")) {
        token = strtok(NULL, " ");
        if(token==NULL) {
            strcpy(response_msg, "Unknown Command!");
            return -1;
        }

        position = -1;
        for (int i = 0; i < strlen(token); i++) {
            if (token[i] == '=') {
                position = i;
                break;
            }
        }
        if (position != -1) {
            // Extract the key and value parts
            strncpy(column_identifier, token, position);
            column_identifier[position] = '\0';

            strncpy(identifier_value, token + position + 1, sizeof(identifier_value));
            identifier_value[sizeof(identifier_value) - 1] = '\0';


            // Removing the Semicolon
            int len = strlen(identifier_value);
            if (len > 0 && identifier_value[len - 1] == ';') {
                identifier_value[len - 1] = '\0';
            }

            // TODO

            char fpath[100], fpathnew[100];
            snprintf(fpath, 100, "%s/%s/%s", databasesFolder, current_db, table_name);
            snprintf(fpathnew, 100, "%s/%s/%snew", databasesFolder, current_db, table_name);
            
           FILE* file = fopen(fpath, "r"); // Open the file for reading
            FILE* tempFile = fopen(fpathnew, "w"); // Open a temporary file for writing

            if (file == NULL || tempFile == NULL) {
                strcpy(response_msg, "Error, please try again later!");
                return 1;
            }

            char line[500];
            fgets(line, sizeof(line), file);
            fputs(line, tempFile); // Write the line to the temporary file

            token = strtok(strdup(line), ":"); // Split the columns by ':'
            int columnNumberTarget = 1, identifierTarget = 1, colnumfinish=0, idnumfinish=0;
            while (token != NULL) {
                char* equalsSign = strstr(token, "="); // Find the position of '=' in the column

                if (equalsSign != NULL) {
                    *equalsSign = '\0'; // Replace '=' with '\0' to separate the column name

                    if (strcmp(token, column_name) == 0) {
                        colnumfinish=1;
                    }

                    if (strcmp(token, column_identifier) == 0) {
                        idnumfinish=1;
                    }
                }

                token = strtok(NULL, ":");
                if(!colnumfinish) columnNumberTarget++;
                if(!idnumfinish) identifierTarget++;
            }            
            
            while (fgets(line, sizeof(line), file)) {
                char* token = strtok(strdup(line), ":"); // Split the line by ':'
                int columnNumber = 1;
                int istarget = 0; // Flag to determine if the line has been modified
                if (strlen(line) == 0 || line[0] == '\n') continue;

                while (token != NULL) {
                    sanitizeString(token);
                    sanitizeString(identifier_value);

                    if (columnNumber == identifierTarget && strcmp(token, identifier_value) == 0) {
                        istarget = 1; // Set the flag to indicate modification
                        break;
                    }
                    columnNumber++;
                    token = strtok(NULL, ":");
                }


                token = strtok(strdup(line), ":"); // Split the line by ':'
                columnNumber = 1;
                while (token != NULL) {

                    if (columnNumber == columnNumberTarget && istarget) {
                        strcpy(token, column_value);
                    }

                    strcat(token, "\n");
                    fputs(token, tempFile); // Write the token to the temporary file

                    token = strtok(NULL, ":");
                    if(token!=NULL) fputs(":", tempFile); // Add ':' delimiter
                    columnNumber++;
                }

            }
            fclose(file);
            fclose(tempFile);

            remove(fpath); // Remove the original file
            rename(fpathnew, fpath); // Rename the temporary file to the original filename    
            strcpy(response_msg, "Data Update!");
            return 0;
        }
    } else {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    strcpy(response_msg, "Erro, please try again later!");
    return -1;
}

void delete(char* token) {
    char table_name[100], column_identifier[100], identifier_value[100];

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    if(strcmp(token, "FROM") != 0) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(table_name, token);

    token = strtok(NULL, " ");
    if(token==NULL) {
        // Removing the Semicolon
        int len = strlen(table_name);
        if (len > 0 && table_name[len - 1] == ';') {
            table_name[len - 1] = '\0';
        }

        char cmd[100];
        snprintf(cmd, 100, "sed -i '1!d' %s/%s/%s", databasesFolder, current_db, table_name);
        system(cmd);

        strcpy(response_msg, "Successfuly deleted!");
        return 0;
    } else if(!strcmp(token, "WHERE")) {
        token = strtok(NULL, " ");
        if(token==NULL) {
            strcpy(response_msg, "Unknown Command!");
            return -1;
        }

        int position = -1;
        for (int i = 0; i < strlen(token); i++) {
            if (token[i] == '=') {
                position = i;
                break;
            }
        }
        if (position != -1) {
            // Extract the key and value parts
            strncpy(column_identifier, token, position);
            column_identifier[position] = '\0';

            strncpy(identifier_value, token + position + 1, sizeof(identifier_value));
            identifier_value[sizeof(identifier_value) - 1] = '\0';


            // Removing the Semicolon
            int len = strlen(identifier_value);
            if (len > 0 && identifier_value[len - 1] == ';') {
                identifier_value[len - 1] = '\0';
            }

            char fpath[100], fpathnew[100];
            snprintf(fpath, 100, "%s/%s/%s", databasesFolder, current_db, table_name);
            snprintf(fpathnew, 100, "%s/%s/%snew", databasesFolder, current_db, table_name);

            FILE* file = fopen(fpath, "r"); // Open the file for reading
            FILE* tempFile = fopen(fpathnew, "w"); // Open a temporary file for writing

            if (file == NULL || tempFile == NULL) {
                strcpy(response_msg, "Error, please try again later!");
                return 1;
            }

            char line[500];
            fgets(line, sizeof(line), file);
            fputs(line, tempFile); // Write the line to the temporary file

            token = strtok(strdup(line), ":"); // Split the columns by ':'
            int columnNumberTarget = 1;
            while (token != NULL) {
                char* equalsSign = strstr(token, "="); // Find the position of '=' in the column

                if (equalsSign != NULL) {
                    *equalsSign = '\0'; // Replace '=' with '\0' to separate the column name

                    if (strcmp(token, column_identifier) == 0) {
                        break;
                    }
                }

                token = strtok(NULL, ":");
                columnNumberTarget++;
            }

            while (fgets(line, sizeof(line), file)) {
                char newLIne[100];
                strcpy(newLIne, line);
                char* token = strtok(line, ":"); // Split the line by ':'
                int columnNumber = 1;
                int deleteLine = 0; // Flag to determine if the line should be deleted

                while (token != NULL) {
                    if (columnNumber == columnNumberTarget && strcmp(token, identifier_value) == 0) {
                        deleteLine = 1; // Set the flag to delete the line
                        break;
                    }

                    token = strtok(NULL, ":");
                    columnNumber++;
                }

                if (!deleteLine) {
                    fputs(newLIne, tempFile); // Write the line to the temporary file
                }
            }

            fclose(file);
            fclose(tempFile);

            remove(fpath); // Remove the original file
            rename(fpathnew, fpath); // Rename the temporary file to the original filename

            strcpy(response_msg, "Successfully deleted!");
            return 0;
        } else {
            return -1;
        }

    }
    strcpy(response_msg, "Unknown Command!");
    return -1;
}

void select_row(char* token) {
    char data[1020] = "| ", table[100];
    char columns[100][100];
    int is_all = 0, ctr = 0;;

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    if(!strcmp(token, "*")) {
        is_all = 1;
        token = strtok(NULL, " ");
    }
    else if(token[strlen(token)-1] != ',') {
        strcpy(columns[ctr], token);
        ctr++;
        token = strtok(NULL, " ");
    }

    while(token[strlen(token)-1] == ',') {
        size_t length = strlen(token);
        if (length > 0 && token[length - 1] == ',') {
            token[length - 1] = '\0';
        }

        strcpy(columns[ctr], token);
        ctr++;

        token = strtok(NULL, " ");
        if(token==NULL) {
            strcpy(response_msg, "Unknown Command!");
            return -1;
        } else if(token[strlen(token)-1] != ',') {
            strcpy(columns[ctr], token);
            ctr++;
            token = strtok(NULL, " ");
            if(token==NULL) {
                strcpy(response_msg, "Unknown Command!");
                return -1;
            }
            break;
        }
    }

    if(strcmp(token, "FROM") != 0) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    size_t length = strlen(token);
    if (length > 0 && token[length - 1] == ';') {
        token[length - 1] = '\0';
    }
    strcpy(table, token);

    token = strtok(NULL, " ");

    int targets[ctr], i=0;

    if(token==NULL) {

        char fpath[100];
        snprintf(fpath, 100, "%s/%s/%s", databasesFolder, current_db, table);
        
        FILE* file = fopen(fpath, "r"); // Open the file for reading

        if (file == NULL) {
            strcpy(response_msg, "Error, please try again later!");
            return 1;
        }

        char line[500];
        fgets(line, sizeof(line), file);

        token = strtok(strdup(line), ":"); // Split the columns by ':'
        int columnNumber = 1;

        while (token != NULL) {
            char cname[100];
            strcpy(cname, token);
            char* equalsSign = strstr(token, "="); // Find the position of '=' in the column
            token[strcspn(token, "\n")] = '\0';

            if (equalsSign != NULL) {
                *equalsSign = '\0'; // Replace '=' with '\0' to separate the column name

                if(is_all) {
                    targets[i]=columnNumber;
                    i++;
                    cname[strcspn(cname, "\n")] = '\0';
                    strcat(data, cname);
                    strcat(data, " | ");
                    
                } else {
                    for(int j=0; j<ctr; j++) {
                        if (strcmp(token, columns[j]) == 0) {
                            targets[i]=columnNumber;
                            i++;

                            cname[strcspn(cname, "\n")] = '\0';
                            strcat(data, cname);
                            strcat(data, " | ");
                        }
                    }
                }
            }
            token = strtok(NULL, ":");
            columnNumber++;
        }
        strcat(data, "\n| ");

        i=0;
        while (fgets(line, sizeof(line), file)) {
            char* token = strtok(line, ":"); // Split the line by ':'
            int columnNumber = 1;

            // Check if the line is empty
            if (strlen(line) == 0 || line[0] == '\n') continue;

            while (token != NULL) {
                if (columnNumber == targets[i] || is_all) {
                    token[strcspn(token, "\n")] = '\0';
                    strcat(data, token); // Update the second column value to 99
                    strcat(data, " | ");
                    i++;
                }


                token = strtok(NULL, ":");
                columnNumber++;
            }
            i=0;
            strcat(data, "\n| ");
        }

        fclose(file);
        strcpy(response_msg, data);
        return 0;
   
    } else if (!strcmp(token, "WHERE")) {
        char id_column[100], id_value[100];
        int id=1;

        token = strtok(NULL, " ");
        if(token==NULL) {
            strcpy(response_msg, "Unknown Command!");
            return -1;
        }

        size_t length = strlen(token);
        if (length > 0 && token[length - 1] == ';') {
            token[length - 1] = '\0';
        }

        token = strtok(token, "=");

        if (token != NULL) {
            strncpy(id_column, token, sizeof(id_column) - 1);

            token = strtok(NULL, "=");

            if (token != NULL) {
                strncpy(id_value, token, sizeof(id_value) - 1);
            } else {
                strcpy(response_msg, "Unknown Command!");
                return -1;
            }
        } else {
            strcpy(response_msg, "Unknown Command!");
            return -1;
        }

        char fpath[100];
        snprintf(fpath, 100, "%s/%s/%s", databasesFolder, current_db, table);
        
        FILE* file = fopen(fpath, "r"); // Open the file for reading

        if (file == NULL) {
            strcpy(response_msg, "Error, please try again later!");
            return 1;
        }

        char line[500];
        fgets(line, sizeof(line), file);

        token = strtok(strdup(line), ":"); // Split the columns by ':'
        int columnNumber = 1;

        while (token != NULL) {
            char cname[100];
            strcpy(cname, token);
            sanitizeString(token);
            char* equalsSign = strstr(token, "="); // Find the position of '=' in the column

            if (equalsSign != NULL) {
                *equalsSign = '\0'; // Replace '=' with '\0' to separate the column name

                if(is_all) {
                    targets[i]=columnNumber;
                    i++;
                    cname[strcspn(cname, "\n")] = '\0';
                    strcat(data, cname);
                    strcat(data, " | ");
                } else {
                    for(int j=0; j<ctr; j++) {
                        if (strcmp(token, columns[j]) == 0) {
                            targets[i]=columnNumber;
                            i++;

                            cname[strcspn(cname, "\n")] = '\0';
                            strcat(data, cname);
                            strcat(data, " | ");
                        }
                    }
                }

                if(!strcmp(id_column, token)) {
                    id = columnNumber;
                }
            }
            token = strtok(NULL, ":");
            columnNumber++;
        }

        i=0;

        while (fgets(line, sizeof(line), file)) {
            char* token = strtok(strdup(line), ":"); // Split the line by ':'
            int columnNumber = 1, is_target = 0;
            if (strlen(line) == 0 || line[0] == '\n') continue;
            while (token != NULL) {
                if (columnNumber == id && !strcmp(token, id_value)) {
                    is_target = 1;
                    strcat(data, "\n| ");
                    break;
                }

                token = strtok(NULL, ":");
                columnNumber++;
            }

            token = strtok(strdup(line), ":"); // Split the line by ':'
            columnNumber = 1;
            while (token != NULL) {
                if ((columnNumber == targets[i] || is_all) && is_target) {
                    sanitizeString(token);
                    strcat(data, token);
                    strcat(data, " | ");
                    i++;
                }


                token = strtok(NULL, ":");
                columnNumber++;
            }

            i=0;
        }

        fclose(file);
        strcpy(response_msg, data);
        return 0;
    } else {
        strcpy(response_msg, "UNKNOWN COMMAND!");
    }       
            
}

// Check for folder databases
int check_databases() {
    // Check for logfile
    FILE* file = fopen("users.log", "a");
    if (file != NULL) fclose(file);

    // Check if the folder exists
    DIR* dir = opendir(databasesFolder);
    if (dir) {
        closedir(dir);
        return 0;
    } else if (mkdir(databasesFolder, 0777) == 0) {
        DIR* new_db_dir = opendir(adminDB);
        if (new_db_dir) {
            closedir(new_db_dir);
            return -0;
        } else {
            char newDBpath[100];
            snprintf(newDBpath, sizeof(newDBpath), "%s/%s", databasesFolder, adminDB);

            if (mkdir(newDBpath, 0777) == 0) {
                char usersFilePath[100];
                snprintf(usersFilePath, sizeof(usersFilePath), "%s/%s", newDBpath, adminDB_users);

                FILE* usersFilePtr = fopen(usersFilePath, "w");
                if (usersFilePtr) {
                    fclose(usersFilePtr);
                }

                char accessesFilePath[100];
                snprintf(accessesFilePath, sizeof(accessesFilePath), "%s/%s", newDBpath, adminDB_accesses);

                FILE* accessesFilePtr = fopen(accessesFilePath, "w");
                if (accessesFilePtr) {
                    fclose(accessesFilePtr);
                }

                return 0;
            } else {
                return -1;
            }
        }
    } else {
        return -1;
    }
}

void print_log(char *cmd) {
    time_t currentTime;
    struct tm *timeInfo;
    char formattedTime[20];
    char log[500] = "", uname[100], input[500] = "";

    // Get the current time
    currentTime = time(NULL);

    // Convert the current time to the local time
    timeInfo = localtime(&currentTime);

    // Format the time string
    strftime(formattedTime, sizeof(formattedTime), "%Y-%m-%d %H:%M:%S", timeInfo);

    // Tokenize the input based on spaces
    char *token = strtok(cmd, " ");
    if(token!=NULL) {
        sanitizeString(token);
        if (!strcmp(token, "SUDO")) strcpy(uname, "root");  
        else if(!strcmp(token, "LOGIN")) return;
        else strcpy(uname, token);  

        token = strtok(NULL, " ");
        strcat(input, token);

        token = strtok(NULL, " ");
        while ((token != NULL)){
            strcat(input, " ");
            strcat(input, token);
            token = strtok(NULL, " ");
        }
    }

    snprintf(log, 500, "%s:%s:%s", formattedTime, uname, input);
    

    // Save to users.log
    FILE* file = fopen("users.log", "a");
    if (file == NULL) return;
    fprintf(file, "%s\n", log);
    fclose(file);
}

void dump(char* token) {
    char db_name[100], dump_file[100], query[1024];

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(db_name, token);

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(dump_file, token);

    // if (!has_access(db_name)){
    //     if(!is_super_user) {
    //         strcpy(response_msg, "Only Super User or Granted User can access!");
    //         return -1;
    //     }
    // }

    // // CHECK FOLDER BACKUP
    // const char* folderName = "backup";
    // struct stat st;
    // if (stat(folderName, &st) == -1) {
    //     if (mkdir(folderName, 0700) != 0) {
    //         strcpy(response_msg, "Error!");
    //         return 1;
    //     }
    // } else {
    //         strcpy(response_msg, "Error!");
    //         return 1;
    // }

    // // Check if the db folder exists
    // char path[500];
    // snprintf(path, 500,"%s/%s", databasesFolder, db_name);
    // printf("%s %s\n", path, db_name);

    // DIR* new_db_dir = opendir(path);
    // if (!new_db_dir) {
    //     strcpy(response_msg, "The database doesn't exist!");
    //     closedir(new_db_dir)
    //     return -1;
    // }   

    // while ((entry = readdir(new_db_dir)) != NULL) {
    //     char filepath[256];
    //     snprintf(filepath, sizeof(filepath), "%s/%s/%s", databasesFolder,db_name,entry->d_name);

    //     if (stat(filepath, &filestat) == 0 && S_ISREG(filestat.st_mode)) {
    //         strncpy(query, 1024, "DROP TABLE %s\n;", entry->d_name);
            
    //         char new_query[200];
    //         char fpath[100];
    //         snprintf(fpath, 100, "%s/%s/%s", databasesFolder, db_name, entry->d_name);
    //         FILE* file = fopen(fpath, "r"); // Open the file for reading

    //         if (file == NULL) {
    //             strcpy(response_msg, "Error, please try again later!");
    //             return 1;
    //         }

    //         char line[500];
    //         fgets(line, sizeof(line), file);

            
    //     }
    // }

    // closedir(new_db_dir); // Close the directory
}

int main() {
    check_databases();

     int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[BUFFER_SIZE] = {0};
    char *welcome_message = "Welcome to the server!";

    // Create a daemon process
    pid_t pid;
    pid = fork();
    if(pid!=0) return 0;

    // Creating socket file descriptor
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    // Set socket options to reuse address and port
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    // Bind the socket to the specified port
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    // Start listening for incoming connections
    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    printf("Server daemon is running and listening on port %d...\n", PORT);

    while(1) {
        // Accept client connections
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }

        // Receive and send messages to the client
        while (1) {
            memset(buffer, 0, sizeof(buffer));

            // Receive a message from the client
            valread = read(new_socket, buffer, BUFFER_SIZE);

            // Check if the client has closed the connection
            if (valread == 0) {
                break;
            }

            char input[MAX_INPUT_LENGTH];
            strcpy(input, buffer);
            
            char* token;

            // Remove trailing newline character
            input[strcspn(input, "\n")] = '\0';

            // Log User Command
            print_log(strdup(input));

            // Tokenize the input based on spaces
            token = strtok(input, " ");
            if(token!=NULL) {
                sanitizeString(token);
                if (!strcmp(token, "SUDO")){
                    is_super_user = 1;   
                } else if(!strcmp(token, "LOGIN")){
                    token = strtok(NULL, " ");
                    char login_uname[100];
                    strcpy(login_uname, token);

                    token = strtok(NULL, " ");
                    char login_pw[100];
                    strcpy(login_pw, token);

                    if(login(login_uname, login_pw)) {
                        send(new_socket, "GRANTED\n", strlen("GRANTED\n"), 0);
                    } else {
                        send(new_socket, "REJECTED\n", strlen("REJECTED\n"), 0);
                    }
                    continue;
                } else {
                    is_super_user=0;
                    strcpy(current_user, token);
                }
                token = strtok(NULL, " ");
            }

            if (token != NULL) {
                if(!strcmp(token, "CREATE")){
                    token = strtok(NULL, " ");
                    if(token==NULL){
                        strcpy(response_msg, "Unknown Command!");
                    }
                    if(!strcmp(token, "USER")) {
                        create_user(token);
                    } else if(!strcmp(token, "DATABASE")) {
                        create_db(token);
                        printf("%s", response_msg);
                    } else if(!strcmp(token, "TABLE")) {
                        create_table(token);
                    } else {
                        strcpy(response_msg, "Unknown Command!");
                    }

                } else if(!strcmp(token, "USE")) {
                    use_db(token);
                } else if(!strcmp(token, "GRANT")) {
                    grant_permission(token);
                } else if(!strcmp(token, "DROP")) {
                    token = strtok(NULL, " ");
                    if(token==NULL){
                        strcpy(response_msg, "Unknown Command!");
                    }
                    if(!strcmp(token, "DATABASE")) {
                        drop_db(token);
                    } else if(!strcmp(token, "TABLE")) {
                        drop_table(token);
                    } else if(!strcmp(token, "COLUMN")) {
                        drop_column(token);
                    } else {
                        strcpy(response_msg, "Unknown Command!");
                    }
                } else if(!strcmp(token, "INSERT")) {
                    insert(token);
                } else if(!strcmp(token, "UPDATE")) {
                    update(token);
                } else if(!strcmp(token, "DELETE")) {
                    delete(token);
                } else if(!strcmp(token, "SELECT")) {
                    select_row(token);
                } else if(!strcmp(token, "DUMP")) {
                    dump(token); 
                }else {
                    strcpy(response_msg, "Unknown Command!");
                }        
            } else {
                strcpy(response_msg, "Unknown Command!");
            }

            // Send an acknowledgment message to the client
            // strcat(response_msg, "\n");
            send(new_socket, response_msg, strlen(response_msg), 0);
        }

        close(new_socket);  // Close the connected socket
    }

    return 0;
}
