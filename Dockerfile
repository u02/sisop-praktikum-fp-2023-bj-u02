FROM alpine:latest                                             

WORKDIR /app

COPY database/. /app

COPY client/. /app
                                                               
RUN apk update && apk add build-base

RUN gcc -o database database.c

RUN gcc -o client client.c

EXPOSE 8888

CMD sh -c './database & (sleep 2 && echo "USE test; SELECT * FROM table1") | ./client'
