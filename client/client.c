#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 8888
#define BUFFER_SIZE 1024
char *username = NULL;
char *password = NULL;
int is_sudo = 0;
int is_redirect = 0;

int main(int argc, char *argv[]) {

    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[BUFFER_SIZE] = {0};
    char *server_ip = "127.0.0.1";  // Replace with the actual server IP address
    char *database;
    char *backupFile;

    sleep(5);

    // Create socket file descriptor
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    // Convert IPv4 and IPv6 addresses from text to binary form
    if (inet_pton(AF_INET, server_ip, &serv_addr.sin_addr) <= 0) {
        perror("inet_pton failed");
        exit(EXIT_FAILURE);
    }

    // Connect to the server
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        perror("connect failed");
        exit(EXIT_FAILURE);
    }

    uid_t euid = geteuid();
    if (euid == 0) {
        is_sudo = 1;
        printf("Logged in as super user!\n");
    }

    // Parse command-line arguments
    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-u") == 0 && i + 1 < argc) {
            username = argv[i + 1];
            i++;
        } else if (strcmp(argv[i], "-p") == 0 && i + 1 < argc) {
            password = argv[i + 1];
            i++;
        }

        if(argc==3) {
            database = argv[3];
            is_redirect = 1;
        }

        if(argc==5){
            backupFile = argv[5];
            is_redirect = 1;
        }
    }

    // Check if the username and password are provided
    if(!is_sudo) {
        char login[100], cmd[100];
        snprintf(login, 100, "LOGIN %s %s", username, password);
        send(sock, login, strlen(login), 0);
        valread = read(sock, cmd, BUFFER_SIZE);
        if(cmd[0]=='G') {
            printf("Logged in as %s!\n", username);
            if(is_redirect) {
                memset(buffer, 0, sizeof(buffer));
                char cmd[100];
                if(is_sudo) {
                    snprintf(cmd, 200, "SUDO USE %s;", database);
                } else {
                    snprintf(cmd, 200, "%s USE %s", username, database);
                }
                send(sock, cmd, strlen(cmd), 0);
                valread = read(sock, buffer, BUFFER_SIZE);
                printf("%s\n", buffer);

                char line[256]; // Assuming a maximum line length of 255 characters
                FIle *file = fopen(backupFile, "r"); // Open the file in read mode

                if (file == NULL) {
                    printf("File Redirection doesn't exist.\n");
                }

                while (fgets(line, sizeof(line), file) != NULL) {
                    line[strcspn(line, "\n")] = '\0';
                    if (strlen(line) == 0 || line[0] == '\n') continue;
                    send(sock, line, strlen(line), 0);
                    valread = read(sock, buffer, BUFFER_SIZE);
                    printf("%s\n", buffer);
                }

                fclose(file); // Close the file
            }
        } else {
            printf("Login Failed! Make sure your data valid!\n");
            close(sock);  // Close the socket
            return 1;
        }
    }

    printf("Connected to the server\n");

    // Receive and send messages to the server
    while (1) {
        memset(buffer, 0, sizeof(buffer));

        printf("oursql:$ ");
        fgets(buffer, BUFFER_SIZE, stdin);
        buffer[strcspn(buffer, "\n")] = 0;

        char cmd[200];
        if(is_sudo) {
            snprintf(cmd, 200, "SUDO %s", buffer);
        } else {
            snprintf(cmd, 200, "%s %s",username, buffer);
        }

        // Check if the user wants to exit
        if (strcmp(buffer, "exit") == 0) {
            printf("Exiting...\n");
            break;
        }

        // Send the message to the server
        send(sock, cmd, strlen(cmd), 0);

        // Receive the acknowledgment message from the server
        valread = read(sock, buffer, BUFFER_SIZE);
        printf("%s\n", buffer);
    }

    close(sock);  // Close the socket
    return 0;
}
