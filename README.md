<h1>FP Praktikum Sistem Operasi</h1>
<h3>Group U02</h3>

| NAME                      | NRP       |
|---------------------------|-----------|
|Pascal Roger Junior Tauran |5025211072 |
|Riski Ilyas                |5025211189 |
|Armstrong Roosevelt Zamzami|5025211191 |


## FP Question
```
Almas D. Afhin adalah seseorang yang bercita-cita menjadi raja bajak laut. Dia tinggal di Desa Keputih. Pamannya, Sua D. Wahyu adalah seorang Infrastructure Engineer yang bekerja untuk Revolutionary Army. Namun, pada suatu hari, World Government yang bermusuhan dengan Revolutionary Army melancarkan Zero-Day attack pada program server Revolutionary Army yang mengakibatkan website Revolutionary Army menjadi tidak bisa diakses.
Karena Ayah Almas jugalah seorang hacker dan maintainer handal, dia berusaha untuk memperbaiki exploit dari server tersebut. Namun, website Revolutionary Army haruslah tetap berjalan, maka dia meminta bantuan Almas untuk membuat server backup sementara program server utama masih dalam perbaikan.
Karena ini merupakan tugas yang cukup susah, Almas meminta bantuan temannya yaitu Gloriyano D. Aniel. 
Kalian di sini berperan sebagai Gloriyano yang akan membantu Almas dalam membuat server.
```
### A
```
Terdapat user dengan username dan password  yang digunakan untuk mengakses database yang merupakan haknya (database yang memiliki akses dengan username tersebut). Namun, jika user merupakan root (sudo), maka bisa mengakses semua database yang ada.
Catatan: Untuk hak tiap user tidak perlu didefinisikan secara rinci, cukup apakah bisa akses atau tidak.
Username, password, dan hak akses database disimpan di suatu database juga, tapi tidak ada user yang bisa akses database tersebut kecuali mengakses menggunakan root.
User root sudah ada dari awal.
```
Solution:<br>
```c

int login(char* username, char* password) {
    FILE* file = fopen("databases/admin/users", "r");
    if (file == NULL) {
        return 0;
    }

    char line[100];
    int loginSuccessful = 0;

    while (fgets(line, sizeof(line), file) != NULL) {
        char* storedUsername = strtok(line, ":");
        char* storedPassword = strtok(NULL, "\n");

        if (storedUsername != NULL && storedPassword != NULL && strcmp(storedUsername, username) == 0 &&
            strcmp(storedPassword, password) == 0) {
            loginSuccessful = 1;
            break;
        }
    }

    fclose(file);

    return loginSuccessful;
}


int has_access(char* database) {
    FILE* file = fopen("databases/admin/accesses", "r");
    if (file == NULL) {
        return 0;
    }

    char line[100];
    int accessGranted = 0;

    while (fgets(line, sizeof(line), file) != NULL) {
        char* username = strtok(line, ":");
        char* db = strtok(NULL, "\n");

        if (username != NULL && db != NULL && strcmp(username, current_user) == 0 && strcmp(db, database) == 0) {
            accessGranted = 1;
            break;
        }
    }

    fclose(file);

    return accessGranted;

}


int create_user(char* token) {
    char username[100], password[100];

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(username, token);

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    if(strcmp(token, "IDENTIFIED")!=0) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    if(strcmp(token, "BY")!=0) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    token = strtok(NULL, " ");
    if(token==NULL){
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(password, token);

    // Removing the Semicolon
    int len = strlen(password);
    if (len > 0 && password[len - 1] == ';') {
        password[len - 1] = '\0';
    }

    if (!is_super_user){
        strcpy(response_msg, "Only Super User can create account!");
        return -1;
    }

    char file_users[100];
    sprintf(file_users, "%s/%s/%s", databasesFolder, adminDB, adminDB_users);

    FILE* file = fopen(file_users, "a");
    if (file == NULL) {
        strcpy(response_msg, "Error, Please try again later!");
        return -1;
    }

    char user_new[100];
    sprintf(user_new, "\n%s:%s", username, password);
    fprintf(file, "%s\n", user_new);
    fclose(file);

    strcpy(response_msg, "User Created!");
    return 0;
}

```
Explanation:
<br>
`login()`
- `int login(char* username, char* password) {`: This line defines the function named `login` that takes two parameters: `username` and `password`. It returns an integer value, indicating the success or failure of the login process.
- `FILE* file = fopen("databases/admin/users", "r");`: This line declares a pointer to a `FILE` structure named `file` and uses the `fopen` function to open a file named "databases/admin/users" in read-only mode ("r"). The file is assumed to contain user data for authentication.
- `if (file == NULL) { return 0; }`: This line checks if the file was successfully opened. If the file pointer is `NULL`, it means the file could not be opened, and the function returns `0`, indicating a login failure.
- `char line[100];`: This line declares a character array named `line` with a size of 100. It will be used to store each line read from the file.
- `int loginSuccessful = 0;`: This line declares an integer variable named `loginSuccessful` and initializes it to `0`. It will be used to track the success or failure of the login process.
- `while (fgets(line, sizeof(line), file) != NULL) {`: This line starts a loop that reads each line from the file using the `fgets` function. The loop continues as long as `fgets` successfully reads a line (returns a non-NULL value).
- `char* storedUsername = strtok(line, ":");`: This line uses the `strtok` function to tokenize the `line` string using the delimiter ":". It extracts the first token, representing the stored username, and assigns it to the `storedUsername` pointer.
- `char* storedPassword = strtok(NULL, "\n");`: This line continues tokenizing the `line` string from where the previous `strtok` call left off. It extracts the next token, representing the stored password, and assigns it to the `storedPassword` pointer. The delimiter "\n" is used to find the end of the password token.
- `if (storedUsername != NULL && storedPassword != NULL && strcmp(storedUsername, username) == 0 && strcmp(storedPassword, password) == 0) {`: This line checks if both `storedUsername` and `storedPassword` are not NULL and if they match the `username` and `password` provided as arguments to the `login` function, respectively. If the conditions are met, it means the login credentials are valid, and the code inside the if statement will execute.
- `loginSuccessful = 1;`: This line sets the `loginSuccessful` variable to `1`, indicating a successful login attempt.
- `break;`: This line breaks the loop, as there is no need to continue searching for a match once a successful login attempt has been found.
- `fclose(file);`: This line closes the file using the `fclose` function, releasing any resources associated with it.
- `return loginSuccessful;`: This line returns the value of `loginSuccessful` as the result of the `login` function. If it is `1`, the login was successful; otherwise, it was not.


`has_access()`
- `int has_access(char* database) {`: This line defines the function `has_access` that takes a parameter `database` (representing the name of the database) and returns an integer value indicating whether access is granted or not.
- `FILE* file = fopen("databases/admin/accesses", "r");`: This line declares a pointer to a `FILE` structure named `file` and uses the `fopen` function to open a file named "databases/admin/accesses" in read-only mode ("r"). The file is assumed to contain information about user accesses to different databases.
- `if (file == NULL) { return 0; }`: This line checks if the file was successfully opened. If the file pointer is `NULL`, it means the file could not be opened, and the function returns `0`, indicating access is not granted.
- `char line[100];`: This line declares a character array named `line` with a size of 100. It will be used to store each line read from the file.
- `int accessGranted = 0;`: This line declares an integer variable named `accessGranted` and initializes it to `0`. It will be used to track whether access to the database is granted or not.
- `while (fgets(line, sizeof(line), file) != NULL) {`: This line starts a loop that reads each line from the file using the `fgets` function. The loop continues as long as `fgets` successfully reads a line (returns a non-NULL value).
- `char* username = strtok(line, ":");`: This line uses the `strtok` function to tokenize the `line` string using the delimiter ":". It extracts the first token, representing the username, and assigns it to the `username` pointer.
- `char* db = strtok(NULL, "\n");`: This line continues tokenizing the `line` string from where the previous `strtok` call left off. It extracts the next token, representing the database name, and assigns it to the `db` pointer. The delimiter "\n" is used to find the end of the database token.
- `if (username != NULL && db != NULL && strcmp(username, current_user) == 0 && strcmp(db, database) == 0) {`: This line checks if both `username` and `db` are not NULL and if they match the current user and the specified `database`, respectively. If the conditions are met, it means the user has access to the database, and the code inside the if statement will execute.
- `accessGranted = 1;`: This line sets the `accessGranted` variable to `1`, indicating that access to the database is granted.
- `break;`: This line breaks the loop since there is no need to continue searching for a match once access has been granted.
- `fclose(file);`: This line closes the file using the `fclose` function, releasing any resources associated with it.
- `return accessGranted;`: This line returns the value of `accessGranted` as the result of the `has_access` function. If it is `1`, access to the database is granted; otherwise, it is not.

`create_user()`

- `char username[100], password[100];`: This line declares character arrays `username` and `password`, each with a size of 100. These arrays will be used to store the username and password of the new user.
- `token = strtok(NULL, " ");`: This line uses the `strtok` function to tokenize the `token` string (passed as a parameter) using the delimiter " ". It extracts the next token after the initial token, which is expected to be the username.
- `if (token == NULL) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This line checks if the `token` is `NULL`, which means there are no more tokens or an error occurred. If so, it sets the response message to "Unknown Command!" and returns `-1`, indicating an error.
- `strcpy(username, token);`: This line copies the value of `token` (username) to the `username` array.
- The following lines (`token = strtok(NULL, " ");`, `if (token == NULL) {...}`, `if (strcmp(token, "IDENTIFIED") != 0) {...}`) are similar to steps 2-4 but for the tokens "IDENTIFIED" and "BY", respectively. They ensure that the correct command structure is followed and handle potential errors.
- `token = strtok(NULL, " ");`: This line extracts the next token, which is expected to be the password.
- `if (token == NULL) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This line checks if the `token` is `NULL`. If so, it sets the response message to "Unknown Command!" and returns `-1`, indicating an error.
- `strcpy(password, token);`: This line copies the value of `token` (password) to the `password` array.
- `int len = strlen(password);`: This line calculates the length of the password string.
- `if (len > 0 && password[len - 1] == ';') { password[len - 1] = '\0'; }`: This block of code checks if the password string ends with a semicolon. If so, it removes the semicolon by replacing it with a null terminator '\0'. This step is performed to handle any accidental trailing semicolons.
- `if (!is_super_user) { strcpy(response_msg, "Only Super User can create an account!"); return -1; }`: This line checks if the `is_super_user` flag is false. If so, it sets the response message to "Only Super User can create an account!" and returns `-1`, indicating an error.
- `char file_users[100];`: This line declares a character array `file_users` with a size of 100. It will be used to store the file path for the user database.
- `sprintf(file_users, "%s/%s/%s", databasesFolder, adminDB, adminDB_users);`: This line uses `sprintf` to format the file path by concatenating `databasesFolder`, `adminDB`, and `adminDB_users` strings together. The resulting file path represents the location where the user account information will be stored.
- `FILE* file = fopen(file_users, "a");`: This line opens the file specified by `file_users` in "append" mode ("a"). If the file does not exist, it will be created. The file will be used to store the user account information.
- `if (file == NULL) { strcpy
(response_msg, "Error, Please try again later!"); return -1; }`: This line checks if the file was successfully opened. If not, it sets the response message to "Error, Please try again later!" and returns `-1`, indicating an error.
- `char user_new[100];`: This line declares a character array `user_new` with a size of 100. It will be used to store the formatted new user information.
- `sprintf(user_new, "\n%s:%s", username, password);`: This line uses `sprintf` to format the new user information by concatenating the `username`, a colon (":"), and the `password` together. The resulting string represents the new user entry to be added to the user database file.
- `fprintf(file, "%s\n", user_new);`: This line writes the formatted new user information (`user_new`) to the file.
- `fclose(file);`: This line closes the file, ensuring that the changes are saved.
- `strcpy(response_msg, "User Created!"); return 0;`: This line sets the response message to "User Created!" and returns `0`, indicating success.


### B
```
Untuk dapat mengakses database yang dia punya, permission dilakukan dengan command. Pembuatan tabel dan semua DML butuh untuk mengakses database terlebih dahulu.
Yang bisa memberikan permission atas database untuk suatu user hanya root.
```
Solution:<br>
```c

int use_db(char* token) {
    char db_name[100];

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(db_name, token);

    // Removing the Semicolon
    int len = strlen(db_name);
    if (len > 0 && db_name[len - 1] == ';') {
        db_name[len - 1] = '\0';
    }

    if (!has_access(db_name)){
        if(!is_super_user) {
            strcpy(response_msg, "Only Super User or Granted User can access!");
            return -1;
        }
    }

    // Check if the db folder exists
    char path[500];
    snprintf(path, 500,"%s/%s", databasesFolder, db_name);
    printf("%s %s\n", path, db_name);

    DIR* new_db_dir = opendir(path);
    if (new_db_dir) {
        closedir(new_db_dir);
        strcpy(response_msg, "Using the database!");
        strcpy(current_db, db_name);
        return 0;
    } else {
        strcpy(response_msg, "The database doesn't exist!");
        return -1;
    }
}

int grant_permission(char* token){
    char db_name[100], username[100];

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    if(strcmp(token, "PERMISSION") != 0) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(db_name, token);

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    if(strcmp(token, "INTO") != 0) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(username, token);

    // Removing the Semicolon
    int len = strlen(username);
    if (len > 0 && username[len - 1] == ';') {
        username[len - 1] = '\0';
    }

    if (!is_super_user){
        strcpy(response_msg, "Only Super User can grant access!");
        return -1;
    }

    char file_accesses[100];
    sprintf(file_accesses, "%s/%s/%s", databasesFolder, adminDB, adminDB_accesses);

    FILE* file = fopen(file_accesses, "a");
    if (file == NULL) {
        strcpy(response_msg, "Error, please try again later!");
        return -1;
    }

    char access_new[100];
    sprintf(access_new, "%s:%s", username, db_name);
    fprintf(file, "%s\n", access_new);
    fclose(file);

    strcpy(response_msg, "Permission has been granted!");
    return 0;
}

```
Explanation:

`use_db()`

- `char db_name[100];`: This line declares a character array `db_name` with a size of 100. It will be used to store the name of the database to be used.
- `token = strtok(NULL, " ");`: This line uses the `strtok` function to tokenize the `token` string (passed as a parameter) using the delimiter " ". It extracts the next token after the initial token, which is expected to be the name of the database.
- `if (token == NULL) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This line checks if the `token` is `NULL`, which means there are no more tokens or an error occurred. If so, it sets the response message to "Unknown Command!" and returns `-1`, indicating an error.
- `strcpy(db_name, token);`: This line copies the value of `token` (database name) to the `db_name` array.
-`int len = strlen(db_name);`: This line calculates the length of the database name string.
- `if (len > 0 && db_name[len - 1] == ';') { db_name[len - 1] = '\0'; }`: This block of code checks if the database name string ends with a semicolon. If so, it removes the semicolon by replacing it with a null terminator '\0'. This step is performed to handle any accidental trailing semicolons.
- `if (!has_access(db_name)) {`: This line checks if the current user has access to the specified database by calling the `has_access` function. If the user does not have access:
- `if (!is_super_user) { strcpy(response_msg, "Only Super User or Granted User can access!"); return -1; }`: This line checks if the user is not a superuser. If so, it sets the response message to "Only Super User or Granted User can access!" and returns `-1`, indicating an error.
- `char path[500];`: This line declares a character array `path` with a size of 500. It will be used to store the path to the database folder.
- `snprintf(path, 500,"%s/%s", databasesFolder, db_name);`: This line uses `snprintf` to format the path by concatenating `databasesFolder`, "/", and the `db_name` together. The resulting path represents the location of the specified database folder.
- `DIR* new_db_dir = opendir(path);`: This line attempts to open the directory specified by `path` using the `opendir` function. It checks if the directory exists.
- `if (new_db_dir) {`: This line checks if the directory was successfully opened.
- `closedir(new_db_dir);`: This line closes the directory using the `closedir` function, releasing any resources associated with it.
- `strcpy(response_msg, "Using the database!");`: This line sets the response message to "Using the database!".
- `strcpy(current_db, db_name);`: This line copies the name of the current database (`db_name`) to the `current_db` variable.
- `return 0;`: This line returns `0`, indicating success.
- `} else {`: This line is the start of the `else` block, indicating that the specified database directory does not exist.
- `strcpy(response_msg, "The database doesn't exist!");`: This line sets the response message to "The database doesn't exist!".
- `return -1;`: This line returns `-1`, indicating an error.

`grant_permission()`

- `char db_name[100], username[100];`: This line declares character arrays `db_name` and `username` with a size of 100. They will be used to store the names of the database and the user, respectively.
- `token = strtok(NULL, " ");`: This line uses the `strtok` function to tokenize the `token` string (passed as a parameter) using the delimiter " ". It extracts the next token after the initial token, which is expected to be "PERMISSION".
- `if (token == NULL) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This line checks if the `token` is `NULL`, which means there are no more tokens or an error occurred. If so, it sets the response message to "Unknown Command!" and returns `-1`, indicating an error.
- `if (strcmp(token, "PERMISSION") != 0) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This line checks if the extracted token is equal to "PERMISSION". If not, it sets the response message to "Unknown Command!" and returns `-1`, indicating an error.
- `token = strtok(NULL, " ");`: This line extracts the next token, which is expected to be the name of the database.
- `if (token == NULL) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This line performs the same check as in line 3 to ensure that a valid token exists.
- `strcpy(db_name, token);`: This line copies the value of the token (database name) to the `db_name` array.
- `token = strtok(NULL, " ");`: This line extracts the next token, which is expected to be "INTO".
- `if (token == NULL) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This line performs the same check as in line 3 to ensure that a valid token exists.
- `if (strcmp(token, "INTO") != 0) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This line checks if the extracted token is equal to "INTO". If not, it sets the response message to "Unknown Command!" and returns `-1`, indicating an error.
- `token = strtok(NULL, " ");`: This line extracts the next token, which is expected to be the username.
- `if (token == NULL) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This line performs the same check as in line 3 to ensure that a valid token exists.
- `strcpy(username, token);`: This line copies the value of the token (username) to the `username` array.
- `int len = strlen(username);`: This line calculates the length of the username string.
- `if (len > 0 && username[len - 1] == ';') { username[len - 1] = '\0'; }`: This block of code checks if the username string ends with a semicolon. If so, it removes the semicolon by replacing it with a null terminator '\0'. This step is performed to handle any accidental trailing semicolons.
- `if (!is_super_user) { strcpy(response_msg, "Only Super User can grant access!"); return -1; }`: This line checks if the user is not a superuser. If so, it sets the response message to "Only Super User can grant access!" and returns `-1`, indicating an error.
- `char file_accesses[100];`: This line declares a character array `file_accesses` with a size of 100. It will be used to store the path to the file containing the accesses.
- `sprintf(file_accesses, "%s/%s/%s", databasesFolder, adminDB, adminDB_accesses);`: This line uses `sprintf` to format the path by concatenating `databasesFolder`, "/", `adminDB`, "/", and `adminDB_accesses` together. The resulting path represents the location of the file containing the accesses.
- `FILE* file = fopen(file_accesses, "a");`: This line opens the file specified by `file_accesses` in "a" (append) mode. If the file does not exist, it will be created. If the file cannot be opened, it sets the response message to "Error, please try again later!" and returns `-1`, indicating an error.
- `char access_new[100];`: This line declares a character array `access_new` with a size of 100. It will be used to store the new access entry.
- `sprintf(access_new, "%s:%s", username, db_name);`: This line uses `sprintf` to format the new access entry by concatenating `username`, ":", and `db_name` together.
- `fprintf(file, "%s\n", access_new);`: This line writes the new access entry to the file, followed by a newline character.
- `fclose(file);`: This line closes the file, releasing any resources associated with it.
- `strcpy(response_msg, "Permission has been granted!");`: This line sets the response message to "Permission has been granted!".
- `return 0;`: This line returns `0`, indicating success.

### C
```
Input penamaan database, tabel, dan kolom hanya angka dan huruf.
Semua user bisa membuat database, otomatis user tersebut memiliki permission untuk database tersebut.
Root dan user yang memiliki permission untuk suatu database untuk bisa membuat tabel untuk database tersebut, tentunya setelah mengakses database tersebut. Tipe data dari semua kolom adalah string atau integer. Jumlah kolom bebas.
Bisa melakukan DROP database, table (setelah mengakses database), dan kolom. Jika sasaran drop ada maka di-drop, jika tidak ada maka biarkan.
```
Solution:<br>
```c

int create_db(char* token) {
    char db_name[100];

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    strcpy(db_name, token);

    // Removing the Semicolon
    int len = strlen(db_name);
    if (len > 0 && db_name[len - 1] == ';') {
        db_name[len - 1] = '\0';
    }

    // Open the 'databases' folder
    DIR* dir = opendir(databasesFolder);
    if (dir) {
        // Check if the db folder exists
        DIR* new_db_dir = opendir(db_name);
        if (new_db_dir) {
            closedir(new_db_dir);
            strcpy(response_msg, "The Database already exists!");
            return -1;
        } else {
            char newDBpath[100];
            snprintf(newDBpath, sizeof(newDBpath), "%s/%s", databasesFolder, db_name);

            if (mkdir(newDBpath, 0777) == 0) {
                strcpy(response_msg, "The database created successfully!");
                return 0;
            } else {
                strcpy(response_msg, "Failed to create database, try again later!");
                return -1;
            }
        }

        closedir(dir);
    } else {
        strcpy(response_msg, "Failed to create database, try again later!");
        return -1;    
    }

}

int create_table(char* token) {
    char table_name[100], table_base[100] = "";

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(table_name, token);

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    SinglyList table_structure;
    slist_init(&table_structure);

    if(token[0]=='('){
        int i, j;
        for (i = 0, j = 0; token[i] != '\0'; i++) {
            if (token[i] != '(') {
                token[j] = token[i];
                j++;
            }
        }
        token[j] = '\0';

        strcat(table_base, token);

        token = strtok(NULL, " ");
        if(token==NULL) {
            strcpy(response_msg, "Unknown Command!");
            return -1;
        }

        while(token[strlen(token) - 1] != ';') {
            int len = strlen(token);
            if (len > 0 && token[len - 1] == ',') {
                token[len - 1] = '\0';
            }
            strcat(table_base, "=");
            strcat(table_base, token);
            strcat(table_base, ":");

            token = strtok(NULL, " ");
            if(token==NULL) {
                strcpy(response_msg, "Unknown Command!");
                return -1;
            }
            strcat(table_base, token);

            token = strtok(NULL, " ");
            if(token==NULL) {
                strcpy(response_msg, "Unknown Command!");
                return -1;
            }
        }

        int len = strlen(token);
        if (len > 0 && token[len - 1] == ';') {
            token[len - 1] = '\0';
        }

        len = strlen(token);
        if (len > 0 && token[len - 1] == ')') {
            token[len - 1] = '\0';
        }

        strcat(table_base, "=");
        strcat(table_base, token);

        char tableFilePath[100];
        snprintf(tableFilePath, sizeof(tableFilePath), "%s/%s/%s", databasesFolder, current_db, table_name);

        FILE* tableFilePtr = fopen(tableFilePath, "w");
        if (tableFilePtr) {
            fclose(tableFilePtr);
            FILE* file = fopen(tableFilePath, "a");
            if (file == NULL) {
                strcpy(response_msg, "Unknown Command!");
                return -1;
            }

            fprintf(file, "%s\n", table_base);
            fclose(file);
        }
        
        strcpy(response_msg, "Table created successfully!");
        return 0;
    } else {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    return 0;
}


void drop_db(char* token) {
    char db_name[100];

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(db_name, token);

    // Removing the Semicolon
    int len = strlen(db_name);
    if (len > 0 && db_name[len - 1] == ';') {
        db_name[len - 1] = '\0';
    }

    // Create the command to remove the directory
    char command[100];
    snprintf(command, sizeof(command), "rm -r %s/%s", databasesFolder, db_name);

    // Execute the command
    int result = system(command);
    if (result == -1) {
        return 1;
    }

    strcpy(response_msg, "Database has been dropped!");
    return 0;
}

void drop_table(char* token) {
    char table_name[100];

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(table_name, token);

    // Removing the Semicolon
    int len = strlen(table_name);
    if (len > 0 && table_name[len - 1] == ';') {
        table_name[len - 1] = '\0';
    }

    char filePath[100];
    snprintf(filePath, sizeof(filePath), "%s/%s/%s", databasesFolder, current_db, table_name);

    int result = remove(filePath);
    if (result != 0) {
        strcpy(response_msg, "Table Doesnt exist!");
        return -1;
    }
    return 0;
}

void drop_column(char* token) {
    char table_name[100], column_name[100];

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(column_name, token);

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    if(strcmp(token, "FROM") != 0) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(table_name, token);

    // Removing the Semicolon
    int len = strlen(table_name);
    if (len > 0 && table_name[len - 1] == ';') {
        table_name[len - 1] = '\0';
    }

    char filePath[100];
    snprintf(filePath, 100,"%s/%s/%s", databasesFolder, current_db, table_name);

    FILE* file = fopen(filePath, "r");

    if (file == NULL) {
        strcpy(response_msg, "Failed to open the table, please try again later!.");
        return -1;
    }

    char line[500];
    if (fgets(line, 500, file) != NULL) {
        char* token = strtok(line, ":");
        int i=0;
        while (token != NULL) {

            i++;
            char firstWord[100];
            int j;

            for (j = 0; token[j] != '=' && token[j] != '\0'; j++) {
                firstWord[j] = token[j];
            }

            firstWord[j] = '\0';

            if(!strcmp(firstWord, column_name)) {
                char cmd[100];
                snprintf(cmd, 100,"awk -F: '{$%d=\"\"; sub(/::/,\":\"); sub(/  /,\" \")}1' %s/%s/%s > temp.odb", i, databasesFolder, current_db, table_name);

                system(cmd);

                snprintf(cmd, 100,"sed 's/ /:/g' temp.odb > temp2.odb", databasesFolder, current_db, table_name);
                system(cmd);

                snprintf(cmd, 100, "awk '{gsub(/(^:|:$)/, \"\")}1' temp2.odb > %s/%s/%s", databasesFolder, current_db, table_name);
                system(cmd);
                remove("temp.odb");
                remove("temp2.odb");

                break;
            }
            
            token = strtok(NULL, ":");
        }
    } else {
        strcpy(response_msg, "Error, please make sure the table or the column exists!");
        return -1;
    }

    fclose(file);
    strcpy(response_msg, "Successfully drop the column!");
    return 0;
}


```
Explanation:

`create_db()`

- `char db_name[100];`: This line declares a character array `db_name` with a size of 100. It will be used to store the name of the new database.
- `token = strtok(NULL, " ");`: This line uses the `strtok` function to tokenize the `token` string (passed as a parameter) using the delimiter " ". It extracts the next token after the initial token, which is expected to be the name of the new database.
- `if (token == NULL) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This line checks if the `token` is `NULL`, which means there are no more tokens or an error occurred. If so, it sets the response message to "Unknown Command!" and returns `-1`, indicating an error.
- `strcpy(db_name, token);`: This line copies the value of the token (database name) to the `db_name` array.
- `int len = strlen(db_name);`: This line calculates the length of the database name string.
- `if (len > 0 && db_name[len - 1] == ';') { db_name[len - 1] = '\0'; }`: This block of code checks if the database name string ends with a semicolon. If so, it removes the semicolon by replacing it with a null terminator '\0'. This step is performed to handle any accidental trailing semicolons.
- `DIR* dir = opendir(databasesFolder);`: This line opens the "databases" folder using the `opendir` function and stores the directory pointer in the `dir` variable.
- `if (dir) {`: This line checks if the directory pointer `dir` is not `NULL`, indicating that the "databases" folder exists.
- `DIR* new_db_dir = opendir(db_name);`: This line attempts to open a directory with the name `db_name`, representing the new database.
- `if (new_db_dir) {`: This line checks if the directory pointer `new_db_dir` is not `NULL`, indicating that the new database directory already exists.
- `closedir(new_db_dir); strcpy(response_msg, "The Database already exists!"); return -1;`: This line closes the directory pointer `new_db_dir`, sets the response message to "The Database already exists!", and returns `-1`, indicating an error.
- `else {`: This line is the start of the else block, which executes if the new database directory does not exist.
- `char newDBpath[100];`: This line declares a character array `newDBpath` with a size of 100. It will be used to store the path of the new database directory.
- `snprintf(newDBpath, sizeof(newDBpath), "%s/%s", databasesFolder, db_name);`: This line uses `snprintf` to format the new database path by concatenating `databasesFolder`, "/", and `db_name` together.
- `if (mkdir(newDBpath, 0777) == 0) {`: This line attempts to create a new directory with the path specified by `newDBpath` and the permissions `0777` (read, write, and execute for all). It checks if the directory creation is successful by comparing the return value to `0`.
- `strcpy(response_msg, "The database created successfully!"); return 0;`: This line sets the response message to "The database created successfully!" and returns `0`, indicating success.
- `else {`: This line is the start of the else block, which executes if the directory creation fails.
- `strcpy(response_msg, "Failed to create database, try again later!"); return -1;`: This line sets the response message to "Failed to create database, try again later!" and returns `-1`, indicating an error.
- `closedir(dir);`: This line closes the directory pointer `dir`, releasing any resources associated with it.
- `} else {`: This line is the start of the else block, which executes if the "databases" folder does not exist.
- `strcpy(response_msg, "Failed to create database, try again later!"); return -1;`: This line sets the response message to "Failed to create database, try again later!" and returns `-1`, indicating an error.

`create_table()`

- `char table_name[100], table_base[100] = "";`: This line declares character arrays `table_name` and `table_base` with sizes of 100. `table_name` will store the name of the new table, and `table_base` will store the table's structure.
- `token = strtok(NULL, " ");`: This line uses the `strtok` function to tokenize the `token` string (passed as a parameter) using the delimiter " ". It extracts the next token after the initial token, which is expected to be the name of the new table.
- `if (token == NULL) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This line checks if the `token` is `NULL`, indicating that there are no more tokens or an error occurred. If so, it sets the response message to "Unknown Command!" and returns `-1`, indicating an error.
- `strcpy(table_name, token);`: This line copies the value of the token (table name) to the `table_name` array.
- `token = strtok(NULL, " ");`: This line uses the `strtok` function again to tokenize the `token` string, extracting the next token. This token is expected to be the table's structure definition.
- `if (token == NULL) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This line checks if the `token` is `NULL`, indicating that there are no more tokens or an error occurred. If so, it sets the response message to "Unknown Command!" and returns `-1`, indicating an error.
- `SinglyList table_structure; slist_init(&table_structure);`: This line initializes a singly linked list `table_structure` to store the structure of the table.
- `if (token[0] == '(') {`: This line checks if the first character of the token is '(', indicating the beginning of the table structure definition.
- `int i, j;`: This line declares variables `i` and `j` for loop iterations and array manipulation.
- `for (i = 0, j = 0; token[i] != '\0'; i++) {`: This line starts a loop to remove any '(' characters from the token, effectively removing the opening parenthesis.
- `if (token[i] != '(') { token[j] = token[i]; j++; }`: This line checks if the current character is not '('. If so, it copies the caracter to the `token` array at position `j` and increments `j`.
- `token[j] = '\0';`: This line adds a null terminator at the end of the `token` string, effectively removing the opening parenthesis.
- `strcat(table_base, token);`: This line appends the modified `token` (table structure definition) to the `table_base` string.
- `token = strtok(NULL, " ");`: This line uses `strtok` to extract the next token after the table structure definition.
- `if (token == NULL) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This line checks if the `token` is `NULL`, indicating that there are no more tokens or an error occurred. If so, it sets the response message to "Unknown Command!" and returns `-1`, indicating an error.
- `while (token[strlen(token) - 1] != ';') {`: This line starts a loop that continues until the token ends with a semicolon, indicating the end of the table structure definition.
- `int len = strlen(token); if (len > 0 && token[len - 1] == ',') { token[len - 1] = '\0'; }`: This line checks if the last character of the token is a comma. If so, it replaces the comma with a null terminator, effectively removing the comma.
- `strcat(table_base, "="); strcat(table_base, token); strcat(table_base, ":");`: This line appends the attribute name, an equal sign, the attribute type, and a colon to the `table_base` string.
- `token = strtok(NULL, " ");`: This line extracts the next token, which is expected to be the next attribute name.
- `if (token == NULL) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This line checks if the `token` is `NULL`, indicating that there are no more tokens or an error occurred. If so, it sets the response message to "Unknown Command!" and returns `-1`, indicating an error.
- `strcat(table_base, token);`: This line appends the attribute name to the `table_base` string.
- `token = strtok(NULL, " ");`: This line extracts the next token, which is expected to be the attribute type.
- `if (token == NULL) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This line checks if the `token` is `NULL`, indicating that there are no more tokens or an error occurred. If so, it sets the response message to "Unknown Command!" and returns `-1`, indicating an error.
- `}`: This line marks the end of the while loop.
- `int len = strlen(token); if (len > 0 && token[len - 1] == ';') { token[len - 1] = '\0'; }`: This line checks if the last character of the token is a semicolon. If so, it replaces the semicolon with a null terminator, effectively removing the semicolon.
- `len = strlen(token); if (len > 0 && token[len - 1] == ')') { token[len - 1] = '\0'; }`: This line checks if the last character of the token is a closing parenthesis. If so, it replaces the closing parenthesis with a null terminator, effectively removing the closing parenthesis.
- `strcat(table_base, "="); strcat(table_base, token);`: This line appends the last attribute name and its type to the `table_base` string.
- `char tableFilePath[100]; snprintf(tableFilePath, sizeof(tableFilePath), "%s/%s/%s", databasesFolder, current_db, table_name);`: This line constructs the file path for the new table using the `databasesFolder`, `current_db`, and `table_name` variables.
- `FILE* tableFilePtr = fopen(tableFilePath, "w");`: This line opens the file at the specified file path in write mode, creating a new file or overwriting an existing one.
- `if (tableFilePtr) { fclose(tableFilePtr); FILE* file = fopen(tableFilePath, "a");`: This line checks if the file was successfully opened. If so, it closes the file pointer `tableFilePtr` and opens the file again in append mode using the file pointer `file`.
- `if (file == NULL) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This line checks if the file pointer `file` is `NULL`, indicating an error occurred when opening the file. If so, it sets the response message to "
Unknown Command!" and returns `-1`, indicating an error.
- `fprintf(file, "%s\n", table_base);`: This line writes the `table_base` string, representing the table's structure, to the file.
- `fclose(file);`: This line closes the file.
- `strcpy(response_msg, "Table created successfully!"); return 0;`: This line sets the response message to "Table created successfully!" and returns `0`, indicating success.

`drop_db()`

- `void drop_db(char* token)`: This is the function definition that takes a token as input. The token is expected to contain the name of the database to be dropped.
- `char db_name[100];`: This declares a character array to store the name of the database.
- `token = strtok(NULL, " ");`: This extracts the next token from the input string. If it is `NULL`, it means there are no more tokens, and the function returns an error.
- `if (token == NULL) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This checks if the `token` is `NULL`, indicating that there are no more tokens or an error occurred. If so, it sets the response message to "Unknown Command!" and returns `-1`, indicating an error.
- `strcpy(db_name, token);`: This copies the database name from the token into the `db_name` variable.
- `int len = strlen(db_name); if (len > 0 && db_name[len - 1] == ';') { db_name[len - 1] = '\0'; }`: This checks if the last character of the `db_name` is a semicolon. If so, it replaces the semicolon with a null terminator, effectively removing the semicolon.
- `char command[100]; snprintf(command, sizeof(command), "rm -r %s/%s", databasesFolder, db_name);`: This creates a command string to remove the directory corresponding to the specified database. The `rm -r` command is used to recursively remove the directory and its contents.
- `int result = system(command);`: This executes the command by calling the `system` function and stores the result.
- `if (result == -1) { return 1; }`: If the result of the system command is `-1`, it indicates an error occurred during the execution. In such a case, the function returns `1`.
- `strcpy(response_msg, "Database has been dropped!"); return 0;`: If the command executes successfully, the response message is set to "Database has been dropped!", and the function returns `0` to indicate success.

`drop_table()`

- `void drop_table(char* token)`: This is the function definition that takes a token as input. The token is expected to contain the name of the table to be dropped.
- `char table_name[100];`: This declares a character array to store the name of the table.
- `token = strtok(NULL, " ");`: This extracts the next token from the input string. If it is `NULL`, it means there are no more tokens, and the function returns an error.
- `if (token == NULL) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This checks if the `token` is `NULL`, indicating that there are no more tokens or an error occurred. If so, it sets the response message to "Unknown Command!" and returns `-1`, indicating an error.
- `strcpy(table_name, token);`: This copies the table name from the token into the `table_name` variable.
- `int len = strlen(table_name); if (len > 0 && table_name[len - 1] == ';') { table_name[len - 1] = '\0'; }`: This checks if the last character of the `table_name` is a semicolon. If so, it replaces the semicolon with a null terminator, effectively removing the semicolon.
- `char filePath[100]; snprintf(filePath, sizeof(filePath), "%s/%s/%s", databasesFolder, current_db, table_name);`: This creates a file path string to locate the table file within the current database.
- `int result = remove(filePath);`: This attempts to remove the file specified by the `filePath` using the `remove` function. The `remove` function is used to delete a file.
- `if (result != 0) { strcpy(response_msg, "Table Doesn't exist!"); return -1; }`: If the result of the `remove` function is not `0`, it indicates that the file deletion was unsuccessful, likely because the table file doesn't exist. In such a case, the response message is set to "Table Doesn't exist!" and the function returns `-1`, indicating an error.
- `return 0;`: If the table file is successfully deleted, the function returns `0` to indicate success.

`drop_column()`

- `void drop_column(char* token)`: This is the function definition that takes a token as input. The token is expected to contain the column name and the table name in the format `COLUMN_NAME FROM TABLE_NAME`.
- `char table_name[100], column_name[100];`: These declarations create character arrays to store the table name and column name, respectively.
- `token = strtok(NULL, " ");`: This extracts the next token from the input string. If it is `NULL`, it means there are no more tokens, and the function returns an error.
- `if (token == NULL) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This checks if the `token` is `NULL`, indicating that there are no more tokens or an error occurred. If so, it sets the response message to "Unknown Command!" and returns `-1`, indicating an error.
- `strcpy(column_name, token);`: This copies the column name from the token into the `column_name` variable.
- `token = strtok(NULL, " ");`: This extracts the next token from the input string, which should be the keyword "FROM".
- `if (token == NULL) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This checks if the `token` is `NULL`, indicating that there are no more tokens or an error occurred. If so, it sets the response message to "Unknown Command!" and returns `-1`, indicating an error.
- `if (strcmp(token, "FROM") != 0) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This checks if the `token` is not equal to the keyword "FROM". If they are not equal, it sets the response message to "Unknown Command!" and returns `-1`, indicating an error.
- `token = strtok(NULL, " ");`: This extracts the next token from the input string, which should be the table name.
- `if (token == NULL) { strcpy(response_msg, "Unknown Command!"); return -1; }`: This checks if the `token` is `NULL`, indicating that there are no more tokens or an error occurred. If so, it sets the response message to "Unknown Command!" and returns `-1`, indicating an error.
- `strcpy(table_name, token);`: This copies the table name from the token into the `table_name` variable.
- `int len = strlen(table_name); if (len > 0 && table_name[len - 1] == ';') { table_name[len - 1] = '\0'; }`: This checks if the last character of the `table_name` is a semicolon. If so, it replaces the semicolon with a null terminator, effectively removing the semicolon.
- `char filePath[100]; snprintf(filePath, 100, "%s/%s/%s", databasesFolder, current_db, table_name);`: This creates a file path string to locate the table file within the current database.
- `FILE* file = fopen(filePath, "r");`: This attempts to open the table file in read mode.
- `if (file == NULL) { strcpy(response_msg, "Failed to open the table, please try again later!."); return -1; }`: If the file opening is unsuccessful (the file pointer is `NULL`), it sets the response message to "Failed to open the table, please try again later!" and returns `-1`, indicating an error.
- `char line[500]; if (fgets(line, 500, file) != NULL) { ... } else { strcpy(response_msg, "Error, please make sure the table or the column exists!"); return -1; }`: This block of code reads a line from the file and checks if it is not `NULL`. If a line is successfully read, it proceeds to process the line. Otherwise, it sets the response message to "Error, please make sure the table or the column exists!" and returns `-1`, indicating an error.
- Inside the `while` loop, the code tokenizes the line using `strtok` with ":" as the delimiter to extract each column name.
- `char firstWord[100]; int j; for (j = 0; token[j] != '=' && token[j] != '\0'; j++) { firstWord[j] = token[j]; } firstWord[j] = '\0';`: This code extracts the first word before the "=" character in the token. It assumes that the first word represents a column name.
- `if (!strcmp(firstWord, column_name)) { ... }`: If the extracted column name matches the `column_name` provided as input, it proceeds to remove the column from the table.
- `snprintf(cmd, 100, "awk -F: '{$%d=\"\"; sub(/::/,\":\"); sub(/  /,\" \")}1' %s/%s/%s > temp.odb", i, databasesFolder, current_db, table_name); system(cmd);`: This command uses `awk` to remove the specified column from the table file. It creates a temporary file `temp.odb` containing the modified table.
- `snprintf(cmd, 100, "sed 's/ /:/g' temp.odb > temp2.odb", databasesFolder, current_db, table_name); system(cmd);`: This command uses `sed` to replace spaces with colons in the `temp.odb` file.
- `snprintf(cmd, 100, "awk '{gsub(/(^:|:$)/, \"\")}1' temp2.odb > %s/%s/%s", databasesFolder, current_db, table_name); system(cmd);`: This command uses `awk` to remove leading or trailing colons from the `temp2.odb` file and overwrite the original table file with the modified contents.
- `remove("temp.odb"); remove("temp2.odb");`: These commands remove the temporary files created during the column removal process.
- Finally, the function closes the file, sets the response message to "Successfully drop the column!", and returns `0` to indicate success.


### D
```
Hanya bisa insert satu row per satu command. Insert sesuai dengan jumlah dan urutan kolom.
Hanya bisa update satu kolom per satu command.
Delete data yang ada di tabel.
Select
```
Solution:<br>
`Insert`
```c
void insert(char* token) {
    char table_name[100], row[100] = "\n";

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    if(strcmp(token, "INTO") != 0) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(table_name, token);

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    SinglyList table_structure, types;
    slist_init(&table_structure);
    slist_init(&types);

    if(token[0]=='('){
        int i, j;                

        for (i = 0, j = 0; token[i] != '\0'; i++) {
            if (token[i] != '(') {
                token[j] = token[i];
                j++;
            }
        }
        token[j] = '\0';
        int len = strlen(token);

        if (len > 0 && token[len - 1] == ',') {
            token[len-1] = '\0';
        }

        if(is_string(token)) {
            slist_pushBack(&types, token, "string");
        } else {
            slist_pushBack(&types, token, "int");
        }

        strcat(row, token);
        strcat(row, ":");

        token = strtok(NULL, " ");
        if(token==NULL) {
            strcpy(response_msg, "Unknown Command!");
            return -1;
        }

        while(token[strlen(token) - 1] != ';') {

            if(is_string(token)) {
                slist_pushBack(&types, token, "string");
            } else {
                slist_pushBack(&types, token, "int");
            }

            len = strlen(token);
            if (len > 0 && token[len - 1] == ',') {
                token[len - 1] = '\0';
            }
           
            strcat(row, token);
            strcat(row, ":");

            token = strtok(NULL, " ");
            if(token==NULL) {
                strcpy(response_msg, "Unknown Command!");
                return -1;
            }
        }

        len = strlen(token);
        if (len > 0 && token[len - 1] == ';') {
            token[len - 1] = '\0';
        }

        len = strlen(token);
        if (len > 0 && token[len - 1] == ')') {
            token[len - 1] = '\0';
        }

        if(is_string(token)) {
            slist_pushBack(&types, token, "string");
        } else {
            slist_pushBack(&types, token, "int");
        }
        
        strcat(row, token);

        char tableFilePath[100];
        snprintf(tableFilePath, sizeof(tableFilePath), "%s/%s/%s", databasesFolder, current_db, table_name);
        FILE* tableFilePtr = fopen(tableFilePath, "a");
        if (tableFilePtr) {
            fclose(tableFilePtr);
            FILE* file = fopen(tableFilePath, "a+");
            if (file == NULL) {
                strcpy(response_msg, "Error, please try again later!");
                return -1;
            }

            fprintf(file, "%s\n", row);
            fclose(file);
        }
        
        strcpy(response_msg, "Data inserted!");
        return 0;
    } else {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }    
}
```

`Update`
```c
void update(char* token) {
    char table_name[100], column_name[100], column_identifier[100], column_value[100], identifier_value[100];

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(table_name, token);

    token = strtok(NULL, " ");
    if(token==NULL) {
        return -1;
    }
    if(strcmp(token, "SET") != 0) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    int position = -1;
    for (int i = 0; i < strlen(token); i++) {
        if (token[i] == '=') {
            position = i;
            break;
        }
    }
    if (position != -1) {
        // Extract the key and value parts
        strncpy(column_name, token, position);
        column_name[position] = '\0';

        strncpy(column_value, token + position + 1, sizeof(column_value));
        column_value[sizeof(column_value) - 1] = '\0';

    } else {
        strcpy(response_msg, "Error, please try again later!");
        return -1;
    }

    token = strtok(NULL, " ");
    if(token==NULL) {
        // Removing the Semicolon
        int len = strlen(column_value);
        if (len > 0 && column_value[len - 1] == ';') {
            column_value[len - 1] = '\0';
        }

        char fpath[100], fpathnew[100];
        snprintf(fpath, 100, "%s/%s/%s", databasesFolder, current_db, table_name);
        snprintf(fpathnew, 100, "%s/%s/%snew", databasesFolder, current_db, table_name);
        
        FILE* file = fopen(fpath, "r"); // Open the file for reading
        FILE* tempFile = fopen(fpathnew, "w"); // Open a temporary file for writing

        if (file == NULL || tempFile == NULL) {
            strcpy(response_msg, "Error, make sure you are using the correct database!");
            remove(fpathnew); // Remove the original file
            return 1;
        }

        char line[500];
        fgets(line, sizeof(line), file);
        fputs(line, tempFile); // Write the line to the temporary file

        token = strtok(strdup(line), ":"); // Split the columns by ':'
        int columnNumberTarget = 1;
        while (token != NULL) {
            char* equalsSign = strstr(token, "="); // Find the position of '=' in the column

            if (equalsSign != NULL) {
                *equalsSign = '\0'; // Replace '=' with '\0' to separate the column name

                if (strcmp(token, column_name) == 0) {
                    break;
                }
            }

            token = strtok(NULL, ":");
            columnNumberTarget++;
        }
        
        while (fgets(line, sizeof(line), file)) {
            char* token = strtok(line, ":"); // Split the line by ':'
            int columnNumber = 1;
            char modifiedLine[500] = "";

                // Check if the line is empty
            if (strlen(line) == 0 || line[0] == '\n') continue;

            while (token != NULL) {
                if (columnNumber == columnNumberTarget) {
                    strcat(modifiedLine, column_value); // Update the second column value to 99
                } else {
                    strcat(modifiedLine, token); // Keep the original column value
                }


                token = strtok(NULL, ":");
                if(token!=NULL)strcat(modifiedLine, ":"); // Add ':' delimiter
                columnNumber++;
            }

            strcat(modifiedLine, "\n");
            fputs(modifiedLine, tempFile);
        }

        fclose(file);
        fclose(tempFile);

        remove(fpath); // Remove the original file
        rename(fpathnew, fpath); // Rename the temporary file to the original filename
        strcpy(response_msg, "Data Updated!");
        return 0;
    } else if(!strcmp(token, "WHERE")) {
        token = strtok(NULL, " ");
        if(token==NULL) {
            strcpy(response_msg, "Unknown Command!");
            return -1;
        }

        position = -1;
        for (int i = 0; i < strlen(token); i++) {
            if (token[i] == '=') {
                position = i;
                break;
            }
        }
        if (position != -1) {
            // Extract the key and value parts
            strncpy(column_identifier, token, position);
            column_identifier[position] = '\0';

            strncpy(identifier_value, token + position + 1, sizeof(identifier_value));
            identifier_value[sizeof(identifier_value) - 1] = '\0';


            // Removing the Semicolon
            int len = strlen(identifier_value);
            if (len > 0 && identifier_value[len - 1] == ';') {
                identifier_value[len - 1] = '\0';
            }

            // TODO

            char fpath[100], fpathnew[100];
            snprintf(fpath, 100, "%s/%s/%s", databasesFolder, current_db, table_name);
            snprintf(fpathnew, 100, "%s/%s/%snew", databasesFolder, current_db, table_name);
            
           FILE* file = fopen(fpath, "r"); // Open the file for reading
            FILE* tempFile = fopen(fpathnew, "w"); // Open a temporary file for writing

            if (file == NULL || tempFile == NULL) {
                strcpy(response_msg, "Error, please try again later!");
                return 1;
            }

            char line[500];
            fgets(line, sizeof(line), file);
            fputs(line, tempFile); // Write the line to the temporary file

            token = strtok(strdup(line), ":"); // Split the columns by ':'
            int columnNumberTarget = 1, identifierTarget = 1, colnumfinish=0, idnumfinish=0;
            while (token != NULL) {
                char* equalsSign = strstr(token, "="); // Find the position of '=' in the column

                if (equalsSign != NULL) {
                    *equalsSign = '\0'; // Replace '=' with '\0' to separate the column name

                    if (strcmp(token, column_name) == 0) {
                        colnumfinish=1;
                    }

                    if (strcmp(token, column_identifier) == 0) {
                        idnumfinish=1;
                    }
                }

                token = strtok(NULL, ":");
                if(!colnumfinish) columnNumberTarget++;
                if(!idnumfinish) identifierTarget++;
            }            
            
            while (fgets(line, sizeof(line), file)) {
                char* token = strtok(strdup(line), ":"); // Split the line by ':'
                int columnNumber = 1;
                int istarget = 0; // Flag to determine if the line has been modified
                if (strlen(line) == 0 || line[0] == '\n') continue;

                while (token != NULL) {
                    sanitizeString(token);
                    sanitizeString(identifier_value);

                    if (columnNumber == identifierTarget && strcmp(token, identifier_value) == 0) {
                        istarget = 1; // Set the flag to indicate modification
                        break;
                    }
                    columnNumber++;
                    token = strtok(NULL, ":");
                }


                token = strtok(strdup(line), ":"); // Split the line by ':'
                columnNumber = 1;
                while (token != NULL) {

                    if (columnNumber == columnNumberTarget && istarget) {
                        strcpy(token, column_value);
                    }

                    strcat(token, "\n");
                    fputs(token, tempFile); // Write the token to the temporary file

                    token = strtok(NULL, ":");
                    if(token!=NULL) fputs(":", tempFile); // Add ':' delimiter
                    columnNumber++;
                }

            }
            fclose(file);
            fclose(tempFile);

            remove(fpath); // Remove the original file
            rename(fpathnew, fpath); // Rename the temporary file to the original filename    
            strcpy(response_msg, "Data Update!");
            return 0;
        }
    } else {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    strcpy(response_msg, "Erro, please try again later!");
    return -1;
}
```

`Delete`
```c
void delete(char* token) {
    char table_name[100], column_identifier[100], identifier_value[100];

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    if(strcmp(token, "FROM") != 0) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    strcpy(table_name, token);

    token = strtok(NULL, " ");
    if(token==NULL) {
        // Removing the Semicolon
        int len = strlen(table_name);
        if (len > 0 && table_name[len - 1] == ';') {
            table_name[len - 1] = '\0';
        }

        char cmd[100];
        snprintf(cmd, 100, "sed -i '1!d' %s/%s/%s", databasesFolder, current_db, table_name);
        system(cmd);

        strcpy(response_msg, "Successfuly deleted!");
        return 0;
    } else if(!strcmp(token, "WHERE")) {
        token = strtok(NULL, " ");
        if(token==NULL) {
            strcpy(response_msg, "Unknown Command!");
            return -1;
        }

        int position = -1;
        for (int i = 0; i < strlen(token); i++) {
            if (token[i] == '=') {
                position = i;
                break;
            }
        }
        if (position != -1) {
            // Extract the key and value parts
            strncpy(column_identifier, token, position);
            column_identifier[position] = '\0';

            strncpy(identifier_value, token + position + 1, sizeof(identifier_value));
            identifier_value[sizeof(identifier_value) - 1] = '\0';


            // Removing the Semicolon
            int len = strlen(identifier_value);
            if (len > 0 && identifier_value[len - 1] == ';') {
                identifier_value[len - 1] = '\0';
            }

            char fpath[100], fpathnew[100];
            snprintf(fpath, 100, "%s/%s/%s", databasesFolder, current_db, table_name);
            snprintf(fpathnew, 100, "%s/%s/%snew", databasesFolder, current_db, table_name);

            FILE* file = fopen(fpath, "r"); // Open the file for reading
            FILE* tempFile = fopen(fpathnew, "w"); // Open a temporary file for writing

            if (file == NULL || tempFile == NULL) {
                strcpy(response_msg, "Error, please try again later!");
                return 1;
            }

            char line[500];
            fgets(line, sizeof(line), file);
            fputs(line, tempFile); // Write the line to the temporary file

            token = strtok(strdup(line), ":"); // Split the columns by ':'
            int columnNumberTarget = 1;
            while (token != NULL) {
                char* equalsSign = strstr(token, "="); // Find the position of '=' in the column

                if (equalsSign != NULL) {
                    *equalsSign = '\0'; // Replace '=' with '\0' to separate the column name

                    if (strcmp(token, column_identifier) == 0) {
                        break;
                    }
                }

                token = strtok(NULL, ":");
                columnNumberTarget++;
            }

            while (fgets(line, sizeof(line), file)) {
                char newLIne[100];
                strcpy(newLIne, line);
                char* token = strtok(line, ":"); // Split the line by ':'
                int columnNumber = 1;
                int deleteLine = 0; // Flag to determine if the line should be deleted

                while (token != NULL) {
                    if (columnNumber == columnNumberTarget && strcmp(token, identifier_value) == 0) {
                        deleteLine = 1; // Set the flag to delete the line
                        break;
                    }

                    token = strtok(NULL, ":");
                    columnNumber++;
                }

                if (!deleteLine) {
                    fputs(newLIne, tempFile); // Write the line to the temporary file
                }
            }

            fclose(file);
            fclose(tempFile);

            remove(fpath); // Remove the original file
            rename(fpathnew, fpath); // Rename the temporary file to the original filename

            strcpy(response_msg, "Successfully deleted!");
            return 0;
        } else {
            return -1;
        }

    }
    strcpy(response_msg, "Unknown Command!");
    return -1;
}
```

`Select`
```c
void select_row(char* token) {
    char data[1020] = "| ", table[100];
    char columns[100][100];
    int is_all = 0, ctr = 0;;

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    if(!strcmp(token, "*")) {
        is_all = 1;
        token = strtok(NULL, " ");
    }
    else if(token[strlen(token)-1] != ',') {
        strcpy(columns[ctr], token);
        ctr++;
        token = strtok(NULL, " ");
    }

    while(token[strlen(token)-1] == ',') {
        size_t length = strlen(token);
        if (length > 0 && token[length - 1] == ',') {
            token[length - 1] = '\0';
        }

        strcpy(columns[ctr], token);
        ctr++;

        token = strtok(NULL, " ");
        if(token==NULL) {
            strcpy(response_msg, "Unknown Command!");
            return -1;
        } else if(token[strlen(token)-1] != ',') {
            strcpy(columns[ctr], token);
            ctr++;
            token = strtok(NULL, " ");
            if(token==NULL) {
                strcpy(response_msg, "Unknown Command!");
                return -1;
            }
            break;
        }
    }

    if(strcmp(token, "FROM") != 0) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }

    token = strtok(NULL, " ");
    if(token==NULL) {
        strcpy(response_msg, "Unknown Command!");
        return -1;
    }
    size_t length = strlen(token);
    if (length > 0 && token[length - 1] == ';') {
        token[length - 1] = '\0';
    }
    strcpy(table, token);

    token = strtok(NULL, " ");

    int targets[ctr], i=0;

    if(token==NULL) {

        char fpath[100];
        snprintf(fpath, 100, "%s/%s/%s", databasesFolder, current_db, table);
        
        FILE* file = fopen(fpath, "r"); // Open the file for reading

        if (file == NULL) {
            strcpy(response_msg, "Error, please try again later!");
            return 1;
        }

        char line[500];
        fgets(line, sizeof(line), file);

        token = strtok(strdup(line), ":"); // Split the columns by ':'
        int columnNumber = 1;

        while (token != NULL) {
            char cname[100];
            strcpy(cname, token);
            char* equalsSign = strstr(token, "="); // Find the position of '=' in the column
            token[strcspn(token, "\n")] = '\0';

            if (equalsSign != NULL) {
                *equalsSign = '\0'; // Replace '=' with '\0' to separate the column name

                if(is_all) {
                    targets[i]=columnNumber;
                    i++;
                    cname[strcspn(cname, "\n")] = '\0';
                    strcat(data, cname);
                    strcat(data, " | ");
                    
                } else {
                    for(int j=0; j<ctr; j++) {
                        if (strcmp(token, columns[j]) == 0) {
                            targets[i]=columnNumber;
                            i++;

                            cname[strcspn(cname, "\n")] = '\0';
                            strcat(data, cname);
                            strcat(data, " | ");
                        }
                    }
                }
            }
            token = strtok(NULL, ":");
            columnNumber++;
        }
        strcat(data, "\n| ");

        i=0;
        while (fgets(line, sizeof(line), file)) {
            char* token = strtok(line, ":"); // Split the line by ':'
            int columnNumber = 1;

            // Check if the line is empty
            if (strlen(line) == 0 || line[0] == '\n') continue;

            while (token != NULL) {
                if (columnNumber == targets[i] || is_all) {
                    token[strcspn(token, "\n")] = '\0';
                    strcat(data, token); // Update the second column value to 99
                    strcat(data, " | ");
                    i++;
                }


                token = strtok(NULL, ":");
                columnNumber++;
            }
            i=0;
            strcat(data, "\n| ");
        }

        fclose(file);
        strcpy(response_msg, data);
        return 0;
   
    } else if (!strcmp(token, "WHERE")) {
        char id_column[100], id_value[100];
        int id=1;

        token = strtok(NULL, " ");
        if(token==NULL) {
            strcpy(response_msg, "Unknown Command!");
            return -1;
        }

        size_t length = strlen(token);
        if (length > 0 && token[length - 1] == ';') {
            token[length - 1] = '\0';
        }

        token = strtok(token, "=");

        if (token != NULL) {
            strncpy(id_column, token, sizeof(id_column) - 1);

            token = strtok(NULL, "=");

            if (token != NULL) {
                strncpy(id_value, token, sizeof(id_value) - 1);
            } else {
                strcpy(response_msg, "Unknown Command!");
                return -1;
            }
        } else {
            strcpy(response_msg, "Unknown Command!");
            return -1;
        }

        char fpath[100];
        snprintf(fpath, 100, "%s/%s/%s", databasesFolder, current_db, table);
        
        FILE* file = fopen(fpath, "r"); // Open the file for reading

        if (file == NULL) {
            strcpy(response_msg, "Error, please try again later!");
            return 1;
        }

        char line[500];
        fgets(line, sizeof(line), file);

        token = strtok(strdup(line), ":"); // Split the columns by ':'
        int columnNumber = 1;

        while (token != NULL) {
            char cname[100];
            strcpy(cname, token);
            sanitizeString(token);
            char* equalsSign = strstr(token, "="); // Find the position of '=' in the column

            if (equalsSign != NULL) {
                *equalsSign = '\0'; // Replace '=' with '\0' to separate the column name

                if(is_all) {
                    targets[i]=columnNumber;
                    i++;
                    cname[strcspn(cname, "\n")] = '\0';
                    strcat(data, cname);
                    strcat(data, " | ");
                } else {
                    for(int j=0; j<ctr; j++) {
                        if (strcmp(token, columns[j]) == 0) {
                            targets[i]=columnNumber;
                            i++;

                            cname[strcspn(cname, "\n")] = '\0';
                            strcat(data, cname);
                            strcat(data, " | ");
                        }
                    }
                }

                if(!strcmp(id_column, token)) {
                    id = columnNumber;
                }
            }
            token = strtok(NULL, ":");
            columnNumber++;
        }

        i=0;

        while (fgets(line, sizeof(line), file)) {
            char* token = strtok(strdup(line), ":"); // Split the line by ':'
            int columnNumber = 1, is_target = 0;
            if (strlen(line) == 0 || line[0] == '\n') continue;
            while (token != NULL) {
                if (columnNumber == id && !strcmp(token, id_value)) {
                    is_target = 1;
                    strcat(data, "\n| ");
                    break;
                }

                token = strtok(NULL, ":");
                columnNumber++;
            }

            token = strtok(strdup(line), ":"); // Split the line by ':'
            columnNumber = 1;
            while (token != NULL) {
                if ((columnNumber == targets[i] || is_all) && is_target) {
                    sanitizeString(token);
                    strcat(data, token);
                    strcat(data, " | ");
                    i++;
                }


                token = strtok(NULL, ":");
                columnNumber++;
            }

            i=0;
        }

        fclose(file);
        strcpy(response_msg, data);
        return 0;
    } else {
        strcpy(response_msg, "UNKNOWN COMMAND!");
    }       
            
}
```
Explanation: <br>
`Insert`: <br>
The `Insert` function handles the insertion of data into the a table within `database.c`
- It receives a token as the input parameter.
- It initializes variables, including `table_name` to store the table name and `row` to construct the row of data.
- It uses `strtok` to tokenize the input command and perform various checks and operations based on the tokens.
- It checks if the next token is `INTO` and if not, it sets an error message and `returns -1`.
- It extracts the `table name` from the next token and stores it in the `table_name` variable.
- It initializes two Singly Linked Lists `table_structure` and `types` to store the table structure and data types.
- It processes the tokenized input to extract column names and their corresponding data types.
- It checks if each token represents a string or an integer and adds it to the types list accordingly.
- It constructs the row of data by concatenating the column names and values with a ":" delimiter.
- It opens the file corresponding to the table and appends the constructed row to the file.
- It sets the response message to indicate successful data insertion.
- And finally it returns 0 to indicate successful execution.

`Update`: <br>
The `Update` function is responsible for updating data in a table within a database based on certain conditions. It takes a command token as input and performs various checks and operations.
- First, it extracts the table name from the command and checks if the next token is `SET`. If not, it returns an "Unknown Command" response.
- Next, it extracts the column name and column value to be updated from the command. It splits the token based on the '=' character and stores the column name and value separately. If the token does not contain '=', it returns an error message.
- If the next token is not `WHERE`, it means the update command does not have a condition, and it proceeds to update all rows in the table with the provided column value. It opens the original file and a temporary file for reading and writing, respectively. It reads each line from the original file, finds the target column based on its name, and updates the corresponding value with the provided column value. The modified lines are written to the temporary file. 
- Finally, the original file is replaced with the temporary file, and the response message is set to "Data Updated".
- If the next token is `WHERE`, it means the update command includes a condition. It extracts the column identifier and its corresponding value from the command. It follows a similar process as before, but in addition to updating the column value, it checks if the identifier value matches the specified condition. Only the rows that meet the condition are updated. The rest of the process is the same as the previous case.
- If none of the above conditions are met, it returns an "Unknown Command" response.

`Delete`: <br>
The `delete` function is responsible for deleting data from a table within a database based on certain conditions. It takes a command token as input and performs various checks and operations.
- First, it initializes variables for the table name, column identifier, and identifier value. It checks if the next token is `FROM`. If not, it returns an "Unknown Command" response.
- Next, it extracts the table name from the command and checks if there is another token. If not, it means the command is to delete all rows from the table. It removes any semicolon at the end of the table name and constructs a command using the `sed` utility to delete all lines except the first line (assuming the first line contains column headers). The response message is set to "Successfully deleted".
- If the next token is `WHERE`, it means the delete command includes a condition. It extracts the column identifier and its corresponding value from the command. It follows a similar process as before, but in addition to copying the lines to a temporary file, it checks if the identifier value matches the specified condition. If a match is found, the line is skipped, deleting it. After processing all lines, the original file is replaced with the temporary file. The response message is set to "Successfully deleted".
- If none of the above conditions are met, it returns an "Unknown Command" response.

`Select`: <br>
The `Select` function is responsible for retrieving and displaying data from a table within a database based on specified conditions. It takes a command token as input and performs various checks and operations.
- The function starts by initializing variables, including the `data` string used to store the retrieved data, an array of `columns` to store the column names to be selected, and `is_all` flag to indicate whether all columns should be selected. It also initializes the `ctr` counter variable.
- Next, it checks if the next token is `"*"` to select all columns. If so, it sets the `is_all` flag and retrieves the next token. If the token does not end with a comma, it assumes it is a column name and adds it to the `columns` array. It increments the `ctr` counter and retrieves the next token.
- The function then enters a loop to handle cases where multiple columns are specified separated by commas. It checks if the current token ends with a comma and if so, removes the comma and adds the token as a column name to the `columns` array. It increments the `ctr` counter and retrieves the next token. If the token is `NULL`, it returns an "Unknown Command" response. If the token does not end with a comma, it assumes it is the last column name, adds it to the `columns` array, increments the `ctr` counter, retrieves the next token, and breaks the loop.
- After the column names are processed, the function checks if the next token is `"FROM"`. If not, it returns an "Unknown Command" response.
- It retrieves the table name from the next token, removes any semicolon at the end of the token, and stores it in the `table` variable. It retrieves the next token.
- The function initializes the `targets` array to store the column numbers of the selected columns and an index variable `i` to keep track of the current target index.
- If the next token is `NULL`, it means no conditions are specified, and the function proceeds to retrieve the data from the table. It constructs the file path based on the database folder, current database, and table name. It opens the file for reading and checks if it is valid. It reads the first line, which is assumed to contain column headers.
- It then splits the columns by `':'` and iterates over them. For each column, it checks if it matches the specified columns to be selected (`is_all` flag is set) or if it matches any of the specified column names. If a match is found, it adds the column number to the `targets` array, increments the index `i`, and appends the column name to the `data` string. It appends a delimiter (`" | "`) after each column name.
- Next, it reads the remaining lines from the file and splits them by `':'`. It checks if the line is empty or contains only a newline character and skips it. For each column value, it checks if the column number matches any of the targets (`is_all` flag is set) or if it should be selected based on the specified column names. If it matches, it appends the value to the `data` string, increments the index `i`, and appends a delimiter.
- After processing all lines, it closes the file, copies the `data` string to the `response_msg` variable, and returns 0 to indicate success.
- If the next token is `WHERE`, it means conditions are specified to select specific rows. It extracts the column name and its corresponding value from the token. It follows a similar process as before, but in addition to selecting columns based on the specified column names, it checks if the column number matches the identifier column `id`. If a match is found, it sets the `is_target` flag to indicate that the current row should be selected.
- After processing all lines and selecting the desired column values, it closes the file, copies the data string to the `response_msg` variable, and returns 0 to indicate success.
- If none of the above conditions are met, it sets the response_msg to "UNKNOWN COMMAND!".


### E
```
Setiap command yang dipakai harus dilakukan logging ke suatu file dengan format. Jika yang eksekusi root, maka username root.
```
Solution:<br>
```c
// Check for folder databases
int check_databases() {
    // Check for logfile
    FILE* file = fopen("users.log", "a");
    if (file != NULL) fclose(file);

    // Check if the folder exists
    DIR* dir = opendir(databasesFolder);
    if (dir) {
        closedir(dir);
        return 0;
    } else if (mkdir(databasesFolder, 0777) == 0) {
        DIR* new_db_dir = opendir(adminDB);
        if (new_db_dir) {
            closedir(new_db_dir);
            return -0;
        } else {
            char newDBpath[100];
            snprintf(newDBpath, sizeof(newDBpath), "%s/%s", databasesFolder, adminDB);

            if (mkdir(newDBpath, 0777) == 0) {
                char usersFilePath[100];
                snprintf(usersFilePath, sizeof(usersFilePath), "%s/%s", newDBpath, adminDB_users);

                FILE* usersFilePtr = fopen(usersFilePath, "w");
                if (usersFilePtr) {
                    fclose(usersFilePtr);
                }

                char accessesFilePath[100];
                snprintf(accessesFilePath, sizeof(accessesFilePath), "%s/%s", newDBpath, adminDB_accesses);

                FILE* accessesFilePtr = fopen(accessesFilePath, "w");
                if (accessesFilePtr) {
                    fclose(accessesFilePtr);
                }

                return 0;
            } else {
                return -1;
            }
        }
    } else {
        return -1;
    }
}

void print_log(char *cmd) {
    time_t currentTime;
    struct tm *timeInfo;
    char formattedTime[20];
    char log[500] = "", uname[100], input[500] = "";

    // Get the current time
    currentTime = time(NULL);

    // Convert the current time to the local time
    timeInfo = localtime(&currentTime);

    // Format the time string
    strftime(formattedTime, sizeof(formattedTime), "%Y-%m-%d %H:%M:%S", timeInfo);

    // Tokenize the input based on spaces
    char *token = strtok(cmd, " ");
    if(token!=NULL) {
        sanitizeString(token);
        if (!strcmp(token, "SUDO")) strcpy(uname, "root");  
        else if(!strcmp(token, "LOGIN")) return;
        else strcpy(uname, token);  

        token = strtok(NULL, " ");
        strcat(input, token);

        token = strtok(NULL, " ");
        while ((token != NULL)){
            strcat(input, " ");
            strcat(input, token);
            token = strtok(NULL, " ");
        }
    }

    snprintf(log, 500, "%s:%s:%s", formattedTime, uname, input);
    

    // Save to users.log
    FILE* file = fopen("users.log", "a");
    if (file == NULL) return;
    fprintf(file, "%s\n", log);
    fclose(file);
}
```
Explanation:

- `check_databases()`: This function checks for the existence of a folder named `databases` and creates it if it doesn't exist. It also creates a subfolder named `adminDB` within the `databases` folder and creates two files inside the `adminDB` folder: `adminDB_users` and `adminDB_accesses`.
- It first checks for the existence of a file named `users.log`. If the file exists or can be created, it is immediately closed. This step ensures that the file exists or can be created, without performing any further operations on it.
- Then checks if the folder named `databasesFolder` exists using the `opendir()` function. If the folder exists, it is closed `(closedir(dir))` and the function returns 0 to indicate success.
- If the folder doesn't exist, it attempts to create the `databasesFolder` using the `mkdir()` with the permission code `0777`
- Inside the `databasesFolder` creation block, it checks if the subfolder named `adminDB` exists using `opendir(adminDB)`. If the subfolder exists, it is closed `(closedir(new_db_dir))` and the function returns `-0`. If the subfolder doesn't exist, it creates it using `mkdir(newDBpath, 0777)`.
- After successfully creating the `adminDB` subfolder, it creates two files within it: `adminDB_users` and `adminDB_accesses`
- `print_log(char *cmd)`: This function generates a log entry and appends it to the `users.log` file. It declares variables to hold the current time, a structure to store time information, formatted time string, log string, username, and input string.
- It converts the current time to the local time using `localtime(&currentTime)`. It formats the time string using `strftime()` and stores it in `formattedTime`.
- If the token is not `NULL`, it proceeds to sanitize the token by calling a function `sanitizeString()`and stores it in the `uname` variable. It checks if the token is equal to `SUDO` and if so, assigns `root` as the username. If the token is `LOGIN`, the function returns without further processing. Otherwise, it assigns the token as the username.
- It retrieves the next token and appends it to the input string using `strcat()`.
- It constructs the log string by formatting the log entry with the `formattedTime`, `uname`, and input variables using `snprintf()`.
- If the file is successfully opened, it writes the log string to the file using `fprintf()`, and closes the file using `fclose()`.

### F
```
Harus membuat suatu program terpisah untuk dump database ke command-command yang akan di print ke layar. Untuk memasukkan ke file, gunakan redirection. Program ini tentunya harus melalui proses autentikasi terlebih dahulu. Ini sampai database level saja, tidak perlu sampai tabel. 
```
Solution:<br>
```c
#define PORT 8888
#define BUFFER_SIZE 1024
char *username = NULL;
char *password = NULL;
int is_sudo = 0;
int is_redirect = 0;

int main(int argc, char *argv[]) {

    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[BUFFER_SIZE] = {0};
    char *server_ip = "127.0.0.1";  // Replace with the actual server IP address
    char *database;
    char *backupFile;

    // Create socket file descriptor
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    // Convert IPv4 and IPv6 addresses from text to binary form
    if (inet_pton(AF_INET, server_ip, &serv_addr.sin_addr) <= 0) {
        perror("inet_pton failed");
        exit(EXIT_FAILURE);
    }

    // Connect to the server
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        perror("connect failed");
        exit(EXIT_FAILURE);
    }

    uid_t euid = geteuid();
    if (euid == 0) {
        is_sudo = 1;
        printf("Logged in as super user!\n");
    }

    // Parse command-line arguments
    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-u") == 0 && i + 1 < argc) {
            username = argv[i + 1];
            i++;
        } else if (strcmp(argv[i], "-p") == 0 && i + 1 < argc) {
            password = argv[i + 1];
            i++;
        }

        if(argc==3) {
            database = argv[3];
            is_redirect = 1;
        }

        if(argc==5){
            backupFile = argv[5];
            is_redirect = 1;
        }
    }

    // Check if the username and password are provided
    if(!is_sudo) {
        char login[100], cmd[100];
        snprintf(login, 100, "LOGIN %s %s", username, password);
        send(sock, login, strlen(login), 0);
        valread = read(sock, cmd, BUFFER_SIZE);
        if(cmd[0]=='G') {
            printf("Logged in as %s!\n", username);
            if(is_redirect) {
                memset(buffer, 0, sizeof(buffer));
                char cmd[100];
                if(is_sudo) {
                    snprintf(cmd, 200, "SUDO DUMP %s %s", database, backupFile);
                } else {
                    snprintf(cmd, 200, "%s DUMP %s %s", username, database, backupFile);
                }
                send(sock, cmd, strlen(cmd), 0);
                valread = read(sock, buffer, BUFFER_SIZE);
                fclose(file); // Close the file
            }
        } else {
            printf("Login Failed! Make sure your data valid!\n");
            close(sock);  // Close the socket
            return 1;
        }
    }

    close(sock);  // Close the socket
    return 0;
}

```
Explanation:

- `#define PORT 8888`: This defines the port number to be used for the socket connection.
- `#define BUFFER_SIZE 1024`: This defines the size of the buffer used for receiving data from the server.
- `char *username = NULL;` and `char *password = NULL;`: These variables are used to store the username and password provided as command-line arguments.
- `int is_sudo = 0;` and `int is_redirect = 0;`: These variables are flags to indicate whether the user is a super user (sudo) and whether a redirect operation is requested.
- The `main` function is the entry point of the program.
- The code initializes variables and structures needed for the socket connection.
- It creates a socket using the `socket` system call.
- It sets the server address using the `serv_addr` structure.
- It converts the server IP address from text to binary form using `inet_pton`.
- It connects to the server using the `connect` system call.
- It checks if the program is running with superuser privileges by comparing the effective user ID (`euid`) with 0.
- It parses the command-line arguments using a loop.
- If the `-u` option is provided with a username, it assigns the following argument as the username.
- If the `-p` option is provided with a password, it assigns the following argument as the password.
- If the `argc` is 3, it assumes that a database name is provided, and it assigns the following argument as the database name. It also sets the `is_redirect` flag to 1.
- If the `argc` is 5, it assumes that a backup file name is provided, and it assigns the following argument as the backup file name. It also sets the `is_redirect` flag to 1.
- It checks if the username and password are provided.
- If the program is not running with superuser privileges, it constructs a login command string (`LOGIN username password`) and sends it to the server using the `send` system call. It then reads the server's response using the `read` system call.
- If the server's response starts with 'G', it indicates a successful login. The program prints a success message.
- If the `is_redirect` flag is set, the program constructs a command string for dumping data (`DUMP username database backupFile`) and sends it to the server. It then reads the server's response into the `buffer` variable.
- Finally, the program closes the socket and returns.


### G
```
Kita bisa memasukkan command lewat file dengan redirection di program client. 
```
Solution:<br>
```c
// Check if the username and password are provided
    if(!is_sudo) {
        char login[100], cmd[100];
        snprintf(login, 100, "LOGIN %s %s", username, password);
        send(sock, login, strlen(login), 0);
        valread = read(sock, cmd, BUFFER_SIZE);
        if(cmd[0]=='G') {
            printf("Logged in as %s!\n", username);
            if(is_redirect) {
                memset(buffer, 0, sizeof(buffer));
                char cmd[100];
                if(is_sudo) {
                    snprintf(cmd, 200, "SUDO USE %s;", database);
                } else {
                    snprintf(cmd, 200, "%s USE %s", username, database);
                }
                send(sock, cmd, strlen(cmd), 0);
                valread = read(sock, buffer, BUFFER_SIZE);
                printf("%s\n", buffer);

                char line[256]; // Assuming a maximum line length of 255 characters
                FIle *file = fopen(backupFile, "r"); // Open the file in read mode

                if (file == NULL) {
                    printf("File Redirection doesn't exist.\n");
                }

                while (fgets(line, sizeof(line), file) != NULL) {
                    line[strcspn(line, "\n")] = '\0';
                    if (strlen(line) == 0 || line[0] == '\n') continue;
                    send(sock, line, strlen(line), 0);
                    valread = read(sock, buffer, BUFFER_SIZE);
                    printf("%s\n", buffer);
                }

                fclose(file); // Close the file
            }
        } else {
            printf("Login Failed! Make sure your data valid!\n");
            close(sock);  // Close the socket
            return 1;
        }
    }

    printf("Connected to the server\n");

    // Receive and send messages to the server
    while (1) {
        memset(buffer, 0, sizeof(buffer));

        printf("oursql:$ ");
        fgets(buffer, BUFFER_SIZE, stdin);
        buffer[strcspn(buffer, "\n")] = 0;

        char cmd[200];
        if(is_sudo) {
            snprintf(cmd, 200, "SUDO %s", buffer);
        } else {
            snprintf(cmd, 200, "%s %s",username, buffer);
        }

        // Check if the user wants to exit
        if (strcmp(buffer, "exit") == 0) {
            printf("Exiting...\n");
            break;
        }

        // Send the message to the server
        send(sock, cmd, strlen(cmd), 0);

        // Receive the acknowledgment message from the server
        valread = read(sock, buffer, BUFFER_SIZE);
        printf("%s\n", buffer);
    }

    close(sock);  // Close the socket
    return 0;
```
Explanation:

- First it checks if the variable `is_sudo` is false. If it is false, it proceeds with the login process. It creates a login string using the provided username and password and stores it in the login variable.
- It reads the response from the server into the cmd buffer using the `read()` function, passing the socket sock, the cmd buffer, and the buffer size `BUFFER_SIZE`.
- Then It checks if the first character of cmd is `G`. If it is, it means the login was successful. It prints a success message and proceeds with further operations. If it is not `G`, it means the login failed. It prints a failure message, closes the socket, and returns `1` to indicate failure.
- If redirection is enabled `(is_redirect is true)`, it performs additional operations. It clears the buffer using `memset()` to prepare for new data.
- It creates a new cmd string based on the username, database, and whether it is a sudo operation.
- It opens a file `(backupFile)` in read mode. If the file is `NULL`, it means the file doesn't exist
- After the login and possible redirection steps, it prints a message indicating that the client is connected to the server.
- And then it enters an infinite loop to interact with the server. It clears the buffer using `memset()` to prepare for new data and prompts the user with `oursql:$ `.
- Creates a new cmd string based on the user input and whether it is a sudo operation.
- Checks if the user wants to exit by comparing the buffer with the string `exit`. If it matches, it prints an exit message and breaks out of the loop.
- Sends the cmd string to the server, It reads the response from the server into the buffer and prints the response.
- Finally after the loop is terminated (when the user enters `"exit"`), it closes the socket using `close(sock)`.

### H
```
Jika ada command yang tidak sesuai penggunaannya. Maka akan mengeluarkan pesan error tanpa keluar dari program client.
```
Solution:<br>
```c
strcpy(response_msg, "Unknown Command!");
```
Explanation:

- In each function from the database.c file, it prints a message `"Unknown Command"` everytime a line or syntax is not fit to the conditions that are given.

### I
```
Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan. Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.
Publish Docker Image sistem ke Docker Hub. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.
Untuk memastikan sistem kalian mampu menangani peningkatan penggunaan, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Sukolilo, Keputih, Gebang, Mulyos, dan Semolowaru dan jalankan Docker Compose di sana.
```
Solution:<br>
`Dockerfile`
```dockerfile
FROM alpine:latest                                             

WORKDIR /app

COPY database/. /app

COPY client/. /app
                                                               
RUN apk update && apk add build-base

RUN gcc -o database database.c

RUN gcc -o client client.c

EXPOSE 8888

CMD sh -c './database & (sleep 2 && echo "USE test; SELECT * FROM table1") | ./client'
```
`docker-compose.yml`
```yml
version: "3"
services:
  sukolilo:
    build: 
      context: /home/borgir/Documents/sysop/fp/FP-SISOP/docker/sukolilo
      dockerfile: /home/borgir/Documents/sysop/fp/FP-SISOP/Dockerfile
    deploy:
      replicas: 5
  keputih:
    build: 
      context: /home/borgir/Documents/sysop/fp/FP-SISOP/docker/keputih
      dockerfile: /home/borgir/Documents/sysop/fp/FP-SISOP/Dockerfile
    deploy:
      replicas: 5
  gebang:
    build: 
      context: /home/borgir/Documents/sysop/fp/FP-SISOP/docker/gebang
      dockerfile: /home/borgir/Documents/sysop/fp/FP-SISOP/Dockerfile
    deploy:
      replicas: 5
  mulyos:
    build: 
      context: /home/borgir/Documents/sysop/fp/FP-SISOP/docker/mulyos
      dockerfile: /home/borgir/Documents/sysop/fp/FP-SISOP/Dockerfile
    deploy:
      replicas: 5
  semolowaru:
    build: 
      context: /home/borgir/Documents/sysop/fp/FP-SISOP/docker/semolowaru
      dockerfile: /home/borgir/Documents/sysop/fp/FP-SISOP/Dockerfile
    deploy:
      replicas: 5    

#Sukolilo, Keputih, Gebang, Mulyos, dan Semolowaru
```
`dockerhub.txt`
```txt
https://hub.docker.com/r/pascalrjt/storage-app
```
Explanation:
`Dockerfile`:
- `FROM alpine:latest` sets the base image for the container to be Alpine linux. This was done as alpine linux is very light and small
- `WORKDIR /app` sets the working directory inside the container to /app.
- `COPY database/. /app` and `COPY client/. /app` copies the contents of the `database` and `client` directory into the /app directory inside the container to be used.
- `RUN apk update && apk add build-base` updates the package index of the Alpine package manager and installs the `build-base package`, which provides essential build tools like `gcc` to be able to run our programs.
- `RUN gcc -o database database.c` and `RUN gcc -o client client.c` compiles the `database.c` and `client.c` file using gcc and generates  executables `database` and `client` which will be run by the container.
- `EXPOSE 8888` we expose port 8888 as the database and client will utilize it to comunicate and run the program.
- `CMD sh -c './database & (sleep 2 && echo "USE test; SELECT * FROM table1") | ./client'` runs the `database` program first and the sleeps for 2 seconds in order to allow for the database to have time to set up and then runs the `client` program before executing  `USE test` and `SELECT FROM table1`.

`docker-compose.yml`:
- `services` sets the services to be run when we run docker compose, which in this case is `Sukolilo`, `Keputih`, `Gebang`, `Mulyos`, dan `Semolowaru`
- `Build` specifies the build context and Dockerfile for the each service.
- `context` sets the path to the build context, which is the directory containing the files to be built.
- `dockerfile` sets the path to the Dockerfile
- `Deploy` specifies deployment-related configurations for the service.
- `Replicas: 5` sets the desired number of replicas for the service to 5, indicating that 5 instances of the service will be deployed
